CC = cc -ansi
AR = ar 
RM = rm -f

# add warnings
CFLAGS += -pedantic -Wall -Wextra -Wunused-function -Wunused-parameter -Wunused-label -Wunused-variable -Wwrite-strings -Wunused-macros -Wuninitialized -Wunreachable-code
# silence warning about variadic macros
CFLAGS += -Wno-variadic-macros
# use PIC for shared object compatibility 
CFLAGS += -fPIC
# add debugging
CFLAGS += -O0 -g
# add optimizations 
CFLAGS += -march=native -DNDEBUG -O3 -fomit-frame-pointer
# enable OpenMP
CFLAGS += -fopenmp -fopenmp-simd
# profiler
#CFLAGS += -pg -fno-omit-frame-pointer

# add ISO C99 for expf(), logf()
CFLAGS += -D_ISOC99_SOURCE

# add POSIX for posix_memalign()
CFLAGS += -D_POSIX_C_SOURCE=200112L

# math and pthread library
LIBS += -lm -lpthread
# add LAPACK
LIBS += `pkg-config --libs --static lapack`

# print verbose status information
#CFLAGS += -DVERBOSE
# print timing information
#CFLAGS += -DTIMER

# get headers & objects
headers := $(wildcard src/*.h)
objects := $(patsubst %.c,%.o,$(wildcard src/*.c src/lbfgs/*.c))

# list targets
targets = qsne libqsne.a libqsne.so

all: $(targets)

cleanobj:
	$(RM) $(objects)

clean: cleanobj
	$(RM) $(targets)

qsne: $(objects)
	$(CC) $(CFLAGS) -o "$@" $^ $(LIBS)

libqsne.a: src/util.o src/qsne.o src/matrix.o src/lbfgs/ring_buf.o src/lbfgs/lbfgs_ctx.o src/vln.o
	$(AR) -r -cu "$@" $?

libqsne.so: src/qsne.o
	$(CC) $(CFLAGS) -shared -o "$@" "$<" $(LIBS)

%.o: %.c $(headers)
	$(CC) $(CFLAGS) -c -o "$@" "$<"

man: qsne.1

qsne.1: qsne.1.md
	pandoc --standalone --to man "$<" -o "$@"

.PHONY: all cleanobj clean
.SUFFIXES:
