% QSNE(1) Version 0.9 | QSNE documentation

NAME
====

**qsne** -- Quadratic rate t-SNE optimizer with automatic parameter tuning

SYNOPSIS
========

| **qsne** \[_options_\] [-o _output.tsv_] _input.tsv_

DESCRIPTION
===========

qSNE is a t-SNE implementation with potentially quadratic convergence rate and automatic perplexity tuning.

Options
-------

-d _dims_

:   The dimension of the output (default: 2). Typically **-d 2** or **-d 3** are used for visualizations, but any dimension is possible.

-p _perp_

:   Input perplexity (default: 30). Can also be a range like **-p 20:40**, in which case the perplexity is optimized within the specified range.

-k _iters_

:   Maximum number of iterations (default: 1000). A larger number of iterations guarantees that the optimizer does not terminate prematurely, but this can increase the computational time. Typically, qSNE converges in very few iterations, provided that the stopping tolerance (**-t**) is set correctly.

-t _tol_

:   Minimum relative objective change for considering a stall (default: 0). A larger value make qSNE to terminate early when no sufficient improvement in the objective function is no longer achieved.

-m _rank_

:   Maximum rank of the approximate Hessian (default: 10). A value of 0 implies gradient descent (the original T-SNE algorithm).

-C

:   Compatibility mode. Forces the use of original t-SNE algorithm (**-m 0** and some dubious scaling terms) with gradient descent and momentum term.

-L  _ls_ , -L _ls\_coef_,_ls\_step_

:   Line search parameters (default: 0.5) for L-BFGS. If single value _ls_ is provided, it will be used both for the coefficient _ls\_coef_ and the step size _ls\_step_. Alternatively both coefficient and step size may be specified with a separating comma: e.g. **-L 0.2,0.5**.

-D  _damp_

:   Powell damping parameter used for the Hessian approximation (default: disabled). Typical value is 0.2.

-T  _thr_

:   Number of worker threads used for parallel computations. Default is set to the number of CPU cores detected.

-f  _in\_format_

:   Specifies how the input is encoded: "raw" for plain data (default), or "squaredeuclidean" ("euclidean") for precomputed squared (plain) Euclidean distances.

-i _init.tsv_

:   Specifies the file to read initial conditions from. This will override any setting of **-d**. Default initial condition is a set of the _dims_ major principal components as specified by **-d**.

-o _output.tsv_

:   Specifies the file to write the output. Default is standard output.

Inputs and outputs
------------------

The input and output files are assumed to be tab- or space-separated ASCII text of decimal numbers the rows correspond to samples and the columns variables (such as genes or the different markers of an experiment) unless otherwise mentioned.

EXAMPLES
========

This performs a standard projection of _input.tsv_ into 2D output to _output.tsv_ with perplexity of 30 with the default parameters:

| **qsne** -d 2 -p 30 -o _output.tsv_ _input.tsv_

This performs the above using the original t-SNE algorithm:

| **qsne** -d 2 -p 30 -C -o _output.tsv_ _input.tsv_

As the first one, put perplexity is optimized in the range 20 to 40:

| **qsne** -d 2 -p 20:30 -o _output.tsv_ _input.tsv_

BUGS
====

More flexible settings can be implemented using the C or MATLAB API.

AUTHOR
======

Antti Hakkinen and Juha Koiranen.
