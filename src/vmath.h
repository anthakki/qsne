#ifndef SRC_VMATH_H_
#define SRC_VMATH_H_

/* This header is a trampoline for the math implementation */

/* Intel AVX */
#if !defined(VMATH_DEFINED) && defined(__AVX__)
#	include "vmath-avx.h"
#endif

/* Intel SSE */
#if !defined(VMATH_DEFINED) && defined(__SSE__)
#	include "vmath-sse.h"
#endif

/* Fall back to an ISO C implementation */
#if !defined(VMATH_DEFINED)
#	include "vmath-iso.h"
#endif

#endif /* SRC_VMATH_H_ */
