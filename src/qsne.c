#include "qsne.h"
#include "vmath.h"
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include "util.h"
#include "vln.h"


size_t 
qsne_padcount(size_t count, size_t size) 
{
	return padcount(count, size);	
}


static
void
vv_zero(float *x, size_t n)
{
	size_t i;

	assert(x != NULL || n < 1);

	/* Store zeros */
	for (i = 0; i < n; i += vcountf)
		vstoref(&x[i], vzerof());
}

static
void
vv_acc_pdist(float *r, const float *x, float y, size_t n)
{
	size_t i;
	vfloat yy, t;

	assert(r != NULL || n < 1);
	assert(x != NULL || n < 1);

	/* Broadcast right-hand operand */
	yy = vset1f(y);

	/* Loop */
	for (i = 0; i < n; i += vcountf)
	{
		/* Compute difference & accumulate */
		t = vsubf(vloadf(&x[i]), yy);
		vstoref(&r[i], vfmaddf(t, t, vloadf(&r[i])));
	}
}

void
qsne_pdist(float *d, size_t ldd, const float *x, size_t ldx, size_t m, size_t n)
{
	/* Compute all columns */
	qsne_pdist_part(d, ldd, x, ldx, m, n, 0u, m);
}

void
qsne_pdist_part(float *d, size_t ldd, const float *x, size_t ldx, size_t m, size_t n, size_t a, size_t na)
{
	size_t j, k, b;

	assert(d != NULL || m < 1);
	assert(x != NULL || m < 1 || n < 1);

	b = a + na;

	/* Put in zeros */
	for (j = a; j < b; ++j)
		vv_zero(&d[j * ldd], m);

	/* Loop through the data & accumulate */
	for (k = 0; k < n; ++k)
		for (j = a; j < b; ++j)
			vv_acc_pdist(&d[j * ldd], &x[k * ldx], x[k * ldx + j], m);
}

float
qsne_normal_ent(const float *dists, float t, size_t n) 
{
	float dmin, s_wew, s_ew, w, ew, wew;
	size_t i;

	dmin = (float)HUGE_VAL;
	for (i = 0; i < n; ++i)
		if (dists[i] < dmin)
			dmin = dists[i];

	s_wew = 0.f; /* Sum of w*exp(w) */
	s_ew = 0.f; /* Sum of exp(w) */

	for (i = 0; i < n; ++i)
	{
		w = dists[i] - dmin;
		if (w > 0.)
			w *= t;    /* t=-inf * w=0  = 0 */
		ew = expf(w);

		wew = 0.f;
		if (ew > 0.f)
			wew = w * ew;

		s_wew += wew;
		s_ew += ew;
	}

	return logf(s_ew) - s_wew / s_ew;
}

static
float
qsne_normal_ent_grad(const float *dists, float t, size_t n)
{
	float dmin, s_wew, s_ew, s_wwew, w, ew, wew, wwew;
	size_t i;

	dmin = (float)HUGE_VAL;
	for (i = 0; i < n; ++i)
		if (dists[i] < dmin)
			dmin = dists[i];

	s_wew = 0.f; /* Sum of w*exp(w) */
	s_ew = 0.f; /* Sum of exp(w) */
	s_wwew = 0.f; /* Sum of w*w*exp(w) */

	for (i = 0; i < n; ++i)
	{
		w = dists[i] - dmin;
		if (w > 0.)
			w *= t;
		ew = expf(w);

		wew = 0.f;
		wwew = 0.f;
		if (ew > 0.f)
		{
			wew = w * ew;
			wwew = w * wew;
		}

		s_wew += wew;
		s_ew += ew;
		s_wwew += wwew;
	}

	/*
	 From above, the entropy is : 
	   log(g(t)) - f(t)/g(t)
		with:
		  g(t) = sum( exp(w) )
		f(t) = t*g'(t) = sum( w*exp(w) )
	and
	  h(t) = t^2*g''(t) = sum( w*w*exp(w) )

	The derivative is:
		t * ( g'(t)^2 - g(t)*g''(t) )/g(t)^2
		 = 1/t * ( f(t)^2 / g(t)^2 - h(t) / g(t) )

		Change variable to log(s) where t = -1/(2*s^2) ,
		  normal entropy is 1/2*log(2*pi*exp(1)*s^2) ~ log(s) : 
	dt/d log(s) = -2*t

	*/

	return (( (s_wew/s_ew)*s_wew - s_wwew )) / (( s_ew )) * -2.f;
}

static
float
qsne_sigma_bisect(const float *dists, float tlo, float thi, float ent, size_t n)
{
	float t;

	assert(dists != NULL || n < 1);

	/* NOTE: compute sqrtf(.) separately to avoid overflow */

	/* Bisect */
	t = -sqrtf(-tlo) * sqrtf(-thi);
	while (tlo < t && t < thi)
	{
		if (qsne_normal_ent(dists, t, n) > ent)
			thi = t;
		else
			tlo = t;

		t = -sqrtf(-tlo) * sqrtf(-thi);
	}

	return t;
}

float
qsne_sigma(const float *dists, float ent, size_t n)
{
	float tlo, thi, t;

	assert(dists != NULL || n < 1);

	/* Find lower bound */
	tlo = -.5f;
	while (tlo > -FLT_MAX && (qsne_normal_ent(dists, tlo, n) > ent))
		tlo *= 2.f;

	if (isinf(tlo))
		tlo = -FLT_MAX;

	/* Find upper bound */
	thi = -.5f;
	while (qsne_normal_ent(dists, thi, n) < ent)
		thi *= .5f;

	/* Bisect root */
	t = qsne_sigma_bisect(dists, tlo, thi, ent, n);

	return sqrtf(-.5f / t);
}

float
qsne_sigma_tune(const float *dists, float ent_min, float ent_max, size_t n)
{
	float tlo, thi, t, invg, x, y;

	assert(dists != NULL || n < 1);

	/* Assume negative range is due to roundoff */
	if (!(ent_max >= ent_min))
		ent_min = ent_max = ent_min + .5f * (ent_max - ent_min);

	/* NOTE: Here, t = -1/(2*sigma^2) is used for computational convenience */

	/* Set up bounds */
	tlo = -FLT_MAX;
	thi = -FLT_MIN;

	/* Bisect the values at the interval endpoints */
	tlo = qsne_sigma_bisect(dists, tlo, thi, ent_min, n);
	if (ent_max > ent_min)
		thi = qsne_sigma_bisect(dists, tlo, thi, ent_max, n);
	else
		thi = tlo; /* Fast path for ent_min = ent_max */

	/* Golden ratio search */
	invg = 2.f / (sqrtf(5.f) + 1.f);
	x = thi - (thi - tlo)*invg;
	y = tlo + (thi - tlo)*invg;
	while (tlo < x && y < thi)
	{
		/* Drop lower endpoint */
		if (qsne_normal_ent_grad(dists, x, n) < qsne_normal_ent_grad(dists, y, n))
			tlo = x;
		else
			thi = y;

		/* Pick new interior points in [tlo, thi] */
		x = thi - (thi - tlo)*invg;
		y = tlo + (thi - tlo)*invg;
	}
	t = tlo + .5f * (thi - tlo);

	return sqrtf(-.5f / t);
}

float
qsne_normal(float *p, const float *d, float sigma, size_t m)
{
	vfloat c, z, dmin, tsum, t;
	float dmin1;
	size_t i;

	assert(p != NULL || m < 1);
	assert(d != NULL || m < 1);

	/* Find scaling coefficient */
	c = vset1f(.5f / (sigma * sigma));
	z = vzerof();

	/* Find normalizer */
	dmin1 = (float)HUGE_VAL;
	for (i = 0; i < m; ++i)
		if (d[i] < dmin1)
			dmin1 = d[i];
	dmin = vset1f(dmin1);

	/* Loop */
	tsum = vzerof();
	for (i = 0; i < m; i += vcountf)
	{
		/* Compute unscaled normal density: exp(-.5*d/sigma^2) */
		t = vsubf(dmin, vloadf(&d[i]));
		t = vexpf(vblendf(t, z, vmulf(c, t)));
		tsum = vaddf(tsum, t);

		/* Store */
		vstoref(&p[i], t);
	}

	/* Compute scale */
	return vget1f(vhaddf(tsum));
}

void
qsne_symmetrize(float *dists, size_t ld, size_t rows)
{
	size_t j, i;

	/* TODO: this divides the work asymmetrically 
	    col = j handles both j:th row & col */

	for (j = 0; j < rows; ++j)
		for (i = 0; i < j; ++i)
		{
			float a, b, m;

			a = dists[i+j*ld];
			b = dists[j+i*ld];
			m = a + .5f*(b-a);

			dists[i+j*ld] = m;
			dists[j+i*ld] = m;
		}
}

void
qsne_symmetrize_part(float *dists, size_t ld, size_t rows, size_t col ,size_t ccol, float scal)
{
	size_t j, i;
	(void)rows;

	/* TODO: this divides the work asymmetrically 
	    col = j handles both j:th row & col */

	for (j = col; j < col+ccol; ++j)
		for (i = 0; i < j; ++i)
		{
			float a, b, m;

			a = dists[i+j*ld] * scal;
			b = dists[j+i*ld] * scal;
			m = a + .5f*(b-a);

			dists[i+j*ld] = m;
			((volatile float *)dists)[j+i*ld] = m;   /* TOOD: races possible on AVX/SSE? */
		}
}

float
qsne_cauchy(float *p, const float *d, float gamma, size_t m)
{
	vfloat c, tsum, t;
	size_t i;

	assert(p != NULL || m < 1);
	assert(d != NULL || m < 1);

	/* Find offset */
	c = vset1f(gamma * gamma);

	/* Loop */
	tsum = vzerof();
	for (i = 0; i < m; i += vcountf)
	{
		/* Compute unscaled Cauchy density: 1/( gamma^2 + d ) */
		t = vrcpf(vaddf(c, vloadf(&d[i])));
		tsum = vaddf(tsum, t);

		/* Store */
		vstoref(&p[i], t);
	}

	/* Compute scale */
	return vget1f(vhaddf(tsum));
}

void
qsne_scale(float *x, float s, size_t n)
{
	vfloat c;
	size_t i;

	assert(x != NULL || n < 1);

	/* Compute coefficient */
	c = vset1f(s);

	/* Scale */
	for (i = 0; i < n; i += vcountf)
		vstoref(&x[i], vmulf(c, vloadf(&x[i])));
}

float
qsne_ent(const float *probs, size_t ld, size_t rows, size_t cols)
{
	float s, p;
	size_t j, i;

	s = 0.f;
	for (j = 0; j < cols; ++j)
		for (i = 0; i < rows; ++i)
		{
			p = probs[i + j * ld];
			if (p > 0.f)
				s += p * logf(p);
		}

	return -s;
}

float
qsne_kld(const float *fromprobs, size_t ldfrom, const float *toprobs, size_t ldto, size_t rows, size_t cols)
{
	float s, p, q;
	size_t j, i;

	s = 0.f;
	for (j = 0; j < cols; ++j)
		for (i = 0; i < rows; ++i)
		{
			p = fromprobs[i + j * ldfrom];
			q = toprobs[i + j * ldto];

			if (p > 0.f)
				s += p * logf(p / q);
		}
	return s;
}

float
qsne_kld_vln(const float *fromprobs, size_t ldfrom, const float *toprobs, size_t ldto, size_t rows, size_t cols)
{
	vfloat p, q, s;
	size_t j, i;

	s = vset1f(0.f);
	for (j = 0; j < cols; j++)
		for (i = 0; i < rows; i += vcountf)
		{
			p = vloadf(&fromprobs[i + j * ldfrom]);
			q = vloadf(&toprobs[i + j * ldto]);
			s = vfmaddf(p, vln(vdivf(p, q)), s);
		}
	return vget1f(vhaddf(s));
}

void
qsne_cauchygrad(float *g, size_t ldg, const float *P, size_t ldP,
	const float *Q, size_t ldQ, const float *y, size_t ldy, size_t m, size_t n)
{ qsne_cauchygrad_part(g, ldg, P, ldP, Q, ldQ, y, ldy, m, n, 0, m, 0.f); }

void
qsne_cauchygrad_part(float *g, size_t ldg, const float *P, size_t ldP,
	const float *Q, size_t ldQ, const float *y, size_t ldy, size_t m, size_t n,
		size_t col, size_t ccol, float pscale)
{
	size_t j, i, k;
	vfloat t;

	/* Compute gradient */
	for (k = 0; k < n; ++k)
	{
		float *gk;
		const float *yk;

		gk = &g[k*ldg];
		yk = &y[k*ldy];

		vv_zero(&gk[col], ccol);

		for (j = 0; j < m; ++j)
		{
			const float *Pj, *Qj;
			vfloat ykj;

			Pj = &P[j*ldP];
			Qj = &Q[j*ldQ];

			ykj = vset1f( yk[j] );

			for (i = col; i < col+ccol; i += vcountf)
			{
				/*
				   t = ( Pj[i] - Qj[i] ) * Qj[i];
					 gk[i] += 4.f * t * ( yk[i] - ykj );
				*/
				vfloat Pji = vloadf( &Pj[i] );
				vfloat Qji = vloadf( &Qj[i] );
				if (pscale > 1.f)
					Pji= vmulf(Pji, vset1f(pscale));
				t = vmulf( vsubf( Pji, Qji ), Qji );
				vstoref( &gk[i], vfmaddf( t,
					vsubf( vloadf( &yk[i] ), ykj ), vloadf( &gk[i] ) ) );
			}
		}
	}
}

float
qsne_unscale(float *x, size_t ld, size_t m, size_t n)
{
	vfloat s, t;
	size_t j, i;
	float scale;

	/* 
	 *    find s such that 1/m * (s*x)'*(s*x) is approximately identity
	 */ 

	/* Accumulate stuff here */
	s = vzerof();

	for (j = 0; j < n; ++j)
		/* Compute [ x'*x ]_jj */
		for (i = 0; i < m; i += vcountf)
		{
			t = vloadf(&x[i + j*ld]);
			s = vfmaddf(t, t, s);
		}

	/* Get current scale of the data */
	scale = sqrtf( vget1f(vhaddf(s)) / (float)(m * n) );

	/* Scale data */
	for (j = 0; j < n; ++j)
		qsne_scale( &x[j*ld], 1.f / scale, m );

	/* Return the scale */
	return scale;
}

float
qsne_mean(float *x, size_t m)
{
	size_t i;
	vfloat s;

	/* Zero accumulator */
	s = vzerof();

	/* Loop */
	for (i = 0; i < m; i += vcountf)
		s = vaddf(s, vloadf(&x[i]));

	return vget1f(vhaddf(s)) / m;
}

void
qsne_cov_part(float *c, size_t ldc, const float *means, const float *x, size_t ldx, size_t m, size_t n, size_t a, size_t na)
{
	size_t b, k, j, i;
	vfloat mu, nu, t1, t2, s;
	float bias;

	assert(c != NULL || n < 1);
	assert(x != NULL || m < 1 || n < 1);

	/* Get bounds */
	b = a + na;

	/* Accumulate */
	for (j = a; j < b; ++j)
		for (k = 0; k < n; ++k)
		{
			/* Get means */
			mu = vset1f(means[k]);
			nu = vset1f(means[j]);

			/* Clear accumulator */
			s = vzerof();

			/* Accumulate */
			for (i = 0; i < m; i += vcountf)
			{
				t1 = vsubf(vloadf(&x[i + k*ldx]), mu);
				t2 = vsubf(vloadf(&x[i + j*ldx]), nu);
				s = vfmaddf(t1, t2, s);
			}
			bias = (i-m) * (means[k] * means[j]); /* NB. for zeroed excess */

			/* Get element */
			c[k + j*ldc] = vget1f(vhaddf(s)) - bias;
		}
}

void
qsne_affine_part(float *y, size_t ldy, size_t m, size_t ny, const float *x, size_t ldx, size_t nx, const float *trans, size_t ldt, const float *means, float scale)
{
	size_t j, k, i;
	vfloat beta, mu;

	assert(y != NULL || m*ny < 1);
	assert(x != NULL || m*nx < 1);
	assert(trans != NULL || ny < 1);
	assert(means != NULL || nx < 1);

	/* Loop */
	for (j = 0; j < ny; ++j)
	{
		/* Zero destination */
		vv_zero(&y[j * ldy], m);

		/* Accumulate */
		for (k = 0; k < nx; ++k)
		{
			/* Load transformation */
			beta = vset1f(scale * trans[k + j*ldt]);
			mu = vset1f(means[k]);

			/* Transform */
			for (i = 0; i < m; i += vcountf)
				vstoref(&y[i + j*ldy], vfmaddf(beta, vsubf(vloadf(&x[i + k*ldx]), mu), vloadf(&y[i + j*ldy])));
		}
	}
}
