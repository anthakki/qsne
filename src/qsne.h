#ifndef QSNE_H_
#define QSNE_H_

#include <stddef.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Compute the padding (in elements) required by an array. */
size_t qsne_padcount(size_t count, size_t size);

/* Compute pairwise squared Euclidean distance matrix. */
void qsne_pdist(float *dists, size_t lddi, const float *data, size_t ldda,
	size_t samps, size_t dims);

/* As above, but computes a specified columns only */
void qsne_pdist_part(float *dists, size_t lddi, const float *data, size_t ldda,
	size_t samps, size_t dims, size_t col, size_t ccol);

/* Find a standard deviation for the given entropy for a normal density. */
float qsne_sigma(const float *dists, float ent, size_t n);

/* Find a standard deviation for the given entropy range for a normal density. */
float qsne_sigma_tune(const float *dists, float ent_min, float ent_max, size_t n);

/* Transform squared distances into normal densities.
 * The scaling factor scaling the values to unity is returned. */
float qsne_normal(float *probs, const float *dists, float sigma, size_t samps);

float qsne_normal_ent(const float *dists, float t, size_t n);

/* Symmetrize a matrix by averaging it and its transpose. */
void qsne_symmetrize(float *probs, size_t ld, size_t samps);
/* As above, but some columns only */
void qsne_symmetrize_part(float *probs, size_t ld, size_t samps, size_t col ,size_t ccol, float scal);

/* Transform squared distances into Cauchy densities.
 * The scaling factor is returned. */
float qsne_cauchy(float *probs, const float *dists, float gamma, size_t samps);

/* Scales a vector by a scalar. */
void qsne_scale(float *probs, float scale, size_t samps);

/* Computes entropy */
float qsne_ent(const float *probs, size_t ld, size_t rows, size_t cols);

/* Computes the K-L divergence */
float qsne_kld(const float *fromprobs, size_t ldfrom, const float *toprobs, size_t ldto,
	size_t rows, size_t cols);

/* Computes the K-L divergence with vln */
float qsne_kld_vln(const float *fromprobs, size_t ldfrom, const float *toprobs, size_t ldto,
	size_t rows, size_t cols);

/* Computes the K-L divergence gradient from some density to a Cauchy density
 * with respect to the output locations. */
void qsne_cauchygrad(float *grad, size_t ldgr, const float *fromprobs, size_t ldfrom,
	const float *toprobs, size_t ldtopr, const float *toposs, size_t ldtopo,
		size_t samps, size_t dims);
void qsne_cauchygrad_part(float *grad, size_t ldgr, const float *fromprobs, size_t ldfrom,
	const float *toprobs, size_t ldtopr, const float *toposs, size_t ldtopo,
		size_t samps, size_t dims, size_t col, size_t ccol, float pscale);

/* Scales the data to have unity power, returns the scaling factor */
float qsne_unscale(float *data, size_t ldda, size_t samps, size_t dims);

/* Computes mean of a vector */
float qsne_mean(float *data, size_t samps);

/* Computes the unscaled covariance matrix */
void qsne_cov_part(float *covs, size_t ldc, const float *means,
	const float *data, size_t ldda, size_t samps, size_t dims, size_t col, size_t ccol);

/* Applies an affine transform */
void qsne_affine_part(float *result, size_t ldre, size_t samps, size_t redims,
	const float *data, size_t ldda, size_t dims, const float *trans, size_t ldtr, 
		const float *means, float scale);

#ifdef __cplusplus
}
#endif

#endif /* QSNE_H_ */
