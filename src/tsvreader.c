
#include "tsvreader.h"
#include <assert.h>
#include <stdlib.h>

void
tsvreader_init(tsvreader_t *self)
{
	assert(self != NULL);
	
	/* Set up empty dimensions */
	self->rows_ = 0u;
	self->cols_ = 0u; /* Not known yet */

	/* Set up an empty buffer */
	self->data_.data_ = NULL;
	self->data_.size_ = 0u;
}

void *
tsvreader_buffer_grow_(struct tsvreader_buffer_ *self, size_t count)
{
	size_t size;
	void *data;

	assert(self != NULL);

	/* We have room already? */
	if (!(self->size_ < count))
		return self->data_;

	/* Compute new size */
	size = self->size_;
	if (size > 1048576lu)
		size = size + size / 4u; /* grow conservatively for large sizes */
	else if (size > 0u)
		size = 4u * size;        /* grow aggressively for small sizes */
	else
		size = 1024u;            /* initial size: 4K */

	/* Allocate more memory */
	data = realloc(self->data_, size * sizeof(*self->data_));
	if (data == NULL)
		return NULL;

	/* Update */
	self->data_ = data;
	self->size_ = size;

	return self->data_;
}

static
int
scan_float(float *value, FILE *file)
{
	int ch;

	/* Trim leading whitespace */ 
	do
		ch = getc(file);
	while (ch == ' ' || ch == '\t');

	/* Stop on a newline */
	if (ch == '\n' || ch == '\n')
		return 0;

	/* Parse the float */
	ungetc(ch, file);
	return fscanf(file, "%f", value);
}

int
tsvreader_parse(tsvreader_t *self, FILE *file)
{
	float value;
	size_t t, j;

	assert(self != NULL);
	assert(file != NULL); 

	/* If the number of columns is zero, we must probe it */
	if (!(self->cols_ > 0u))
	{
		/* Loop */
		t = 0;
		while (scan_float(&value, file) == 1)
		{
			/* Check for space in the buffer */
			if (tsvreader_buffer_grow_(&self->data_, t + 1) == NULL)
				return -1;

			/* Put in the value */
			self->data_.data_[t++] = value;
		}

		/* Check for errors */
		if (ferror(file))
			return -1;

		/* Commit */
		++self->rows_;
		self->cols_ = t;
	}

	/* Loop */
	t = self->rows_ * self->cols_;
	while (scan_float(&value, file) == 1)
	{
		/* Check for space in the buffer */
		if (tsvreader_buffer_grow_(&self->data_, t + self->cols_) == NULL)
			return -1;

		/* Put in the first value */
		self->data_.data_[t++] = value;

		/* Read rest of the row */
		for (j = 1; j < self->cols_; ++j)
			if (scan_float(&self->data_.data_[t++], file) != 1)
				return -1;

		/* Check that we're at EOL */
		if (scan_float(&value, file) != 0)
			return -1;

		/* Commit */
		++self->rows_;
	}

	/* Check for read errors */
	if (ferror(file))
		return -1;

	return 0;
}

#undef tsvreader_rows
size_t
tsvreader_rows(const tsvreader_t *self)
{
	assert(self != NULL);
	return tsvreader_rows_(self);
}

#undef tsvreader_cols_
size_t
tsvreader_cols(const tsvreader_t *self)
{
	assert(self != NULL);
	return tsvreader_cols_(self);
}

#undef tsvreader_data_
const float *
tsvreader_data(const tsvreader_t *self)
{
	assert(self != NULL);
	return tsvreader_data_(self);
}

void
tsvreader_deinit(tsvreader_t *self)
{
	/* Give up the buffer if any */
	free(self->data_.data_);
}
