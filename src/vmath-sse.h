#ifndef VMATH_DEFINED
#define VMATH_DEFINED "Intel SSE"

/* Intel SSE math. Requires x86-64 or i386 with SSE support (P III and newer) */
/* See vmath-iso.h for the API details */

#include <xmmintrin.h>
#include <math.h>

#if defined(__SSE3__)
#	include <pmmintrin.h>
#endif
#if defined(__SSE4_1__)
#	include <smmintrin.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef __m128 vfloat;
#define vcountf (4)

#define vzerof() (_mm_setzero_ps())

#define vloadf(p) (_mm_load_ps((p)))
#define vstoref(p, x) (_mm_store_ps((p), (x)))

#define vset1f(x) (_mm_set1_ps((x)))

static __inline float vget1f(__m128 __x) {
	/* Get the lowest word */
	return __x[0];
}

#define vaddf(x, y) (_mm_add_ps((x), (y)))
#define vsubf(x, y) (_mm_sub_ps((x), (y)))
#define vmulf(x, y) (_mm_mul_ps((x), (y)))
#define vdivf(x, y) (_mm_div_ps((x), (y)))

#define vrcpf(x) (_mm_rcp_ps((x)))

#define vfmaddf(a, b, c) (vaddf(vmulf((a), (b)), (c)))

static __inline __m128 vexpf(__m128 __x) {
	/* Implement exp() using the math library-- slow but it's not critical for us */
	__m128 __y;
	int __i;
	for (__i = 0; __i < vcountf; ++__i)
		__y[__i] = expf(__x[__i]);
	return __y;
}

static __inline __m128 vlogf(__m128 __x) {
	/* Implement log() using the math library-- slow but it's not critical for us */
	__m128 __y;
	int __i;
	for (__i = 0; __i < vcountf; ++__i)
		__y[__i] = logf(__x[__i]);
	return __y;
}

static __inline __m128 vhaddf(__m128 __x) {
#if defined(__SSE3__)
	/* Permute data */
	__m128 __y = _mm_shuffle_ps(__x, __x, _MM_SHUFFLE(1,0,3,2)); /* { c,d, a,b } */
	/* Add */
	__m128 __t = _mm_hadd_ps(__x, __y); /* { ab,cd, cd,ab } */
	__t = _mm_hadd_ps(__t, __t);
	return __t;
#else
	/* Without _mm_hadd_ps(), we need more shuffles */
	__m128 __y = _mm_shuffle_ps(__x, __x, _MM_SHUFFLE(2,3,0,1)); /* { b,a, d,c } */
	__m128 __t = _mm_add_ps(__x, __y); /* { ab,ab, cd,cd } */
	return _mm_add_ps(_mm_unpacklo_ps(__t, __t), _mm_unpackhi_ps(__t, __t)); 
#endif
}

static __inline __m128 vblendf(__m128 __x, __m128 __y, __m128 __z) {
#if defined(__SSE4_1__)
	/* SSE 4.1 has blendv() */
	return _mm_blendv_ps(__y, __z, __x);
#else
	/* Implement through bit masking */
	__m128 __m = _mm_cmplt_ps(__x, _mm_setzero_ps());
	return _mm_or_ps(_mm_andnot_ps(__m, __y), _mm_and_ps(__m, __z));
#endif
}

#ifdef __cplusplus
}
#endif 

#endif /* VMATH_DEFINED */
