#ifndef VLN_H_
#define VLN_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "vmath-avx.h"

/* Vectorized natural logarithm. NB. not accurate enough for large scale computations. Remove completely? */

vfloat vln(vfloat x);
vfloat frexp_m(vfloat f);
vint frexp_e(vfloat f);

#ifdef __cplusplus
}
#endif

#endif /* VLN_H_ */
