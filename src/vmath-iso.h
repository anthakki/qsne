#ifndef VMATH_DEFINED
#define VMATH_DEFINED "ISO C99"

/* ISO C99 fallback implementation for vector math */

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Type for a SIMD float */ 
typedef float vfloat;
/* Number of floats per SIMD element */ 
#define vcountf (1)

/* Create an element with zeros */
#define vzerof() ((vfloat)0.f)

/* Load data from a pointer */
#define vloadf(p) (*(p) + 0.f)
/* Store data to a pointer */
#define vstoref(p, x) (*(p) = (x))

/* Create an element with all values equal to a constant */
#define vset1f(x) ((vfloat)x)
/* Get the lowest float of an element */
#define vget1f(x) ((float)x)

/* Add */
#define vaddf(x, y) ((x) + (y))
/* Subtract */
#define vsubf(x, y) ((x) - (y))
/* Multiply */
#define vmulf(x, y) ((x) * (y))
/* Divide */
#define vdivf(x, y) ((x) / (y))

/* Reciprocal */
#define vrcpf(x) (1.f / (x))

/* Fused multiply-add */
#define vfmaddf(x, y, z) ((x) * (y) + (z))

/* Exponential */
#define vexpf(x) (expf(x))
/* Logarithm */
#define vlogf(x) (logf(x))

/* Horizontal add */
#define vhaddf(x) ((x) + 0.f)

/* Blend: selects z iff x is negative and y otherwise */
 /* TODO: NaN behavior is undefined-- I don't use it */
#define vblendf(x, y, z) ((x) < 0.f ? (z) : (y))

#ifdef __cplusplus
}
#endif

#endif /* VMATH_DEFINED */
