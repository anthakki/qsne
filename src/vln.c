#include "vln.h"

/* Based on this: https://golang.org/src/math/log.go */

static const double LCLn2Hi = 6.93147180369123816490e-01; /* 3fe62e42 fee00000 */
static const double LCLn2Lo = 1.90821492927058770002e-10; /* 3dea39ef 35793c76 */
static const double LCL1    = 6.666666666666735130e-01;   /* 3FE55555 55555593 */
static const double LCL2    = 3.999999999940941908e-01;   /* 3FD99999 9997FA04 */
static const double LCL3    = 2.857142874366239149e-01;   /* 3FD24924 94229359 */
static const double LCL4    = 2.222219843214978396e-01;   /* 3FCC71C5 1D8E78AF */
static const double LCL5    = 1.818357216161805012e-01;   /* 3FC74664 96CB03DE */
static const double LCL6    = 1.531383769920937332e-01;   /* 3FC39A09 D078C69F */
static const double LCL7    = 1.479819860511658591e-01;   /* 3FC2F112 DF3E5244 */
/* sqrt(2)/2 */
static const double LCSQRT2D2 = 0.707106781186547573;

vfloat
frexp_m(vfloat f)
{
	vfloat mask_m = (vfloat) vset1i(0x7fffff);
	vfloat mask_e0 = (vfloat) vset1i(0x7e << 23);
	return _mm256_or_ps(_mm256_and_ps(f, mask_m), mask_e0);
}

vint
frexp_e(vfloat f)
{
	static const int expbias = 126;
	vint ee = vsraii((vint)f, 23);
	return vsubi((vint) _mm256_and_ps((vfloat)ee, (vfloat) vset1i(255)), vset1i(expbias));
}

/* TODO: NaN:s and Inf:s */
vfloat
vln(vfloat x)
{
	/* Reduce */
	vint pp;
	vfloat m, mul_m, cmp_m, k, s, s2, s4, t1, t2, R, hfsq, res;

	#define CMP_EQ_OQ 0
	vfloat cmp = _mm256_cmp_ps(x, vset1f(0.f), CMP_EQ_OQ);
	x = vblendf(cmp, x, vset1f(-INFINITY));

	/* Compare */
	m = frexp_m(x);
	pp = (vint) frexp_e(x);
	/* Make vec for multiplying, AND with cmp vector and add 1.f */
	mul_m = vset1f(1.f);
	#define CMP_EQ_LT 1
	cmp_m = _mm256_cmp_ps(m, vset1f((float) LCSQRT2D2), CMP_EQ_LT);
	m = vmulf(m, vaddf(_mm256_and_ps(mul_m, cmp_m), vset1f(1.f)));
	/* Make vec for sub. for thos sat. cond set lane to 1, otherwise 0 */
	pp = vsubi(pp, (vint) _mm256_and_ps((vfloat) vset1i(1), cmp_m));

	m = vsubf(m, vset1f(1));
	k = cvtepii(pp);
	s = vdivf(m, (vaddf(vset1f(2.f),m)));
	s2 = vmulf(s, s);
	s4 = vmulf(s2, s2);
#define M vmulf
#define A vaddf
#define S vsubf
#define F vfmaddf
#define L(i) (vset1f((float) LCL ## i))
	t1 = M(s2, F(s4, F(s4, F(s4, L(7), L(5)), L(3) ), L(1)));
	t2 = M(s4, F(s4, F(s4, L(6),L(4)),L(2)));

	R = A(t1, t2);
	hfsq = M(vset1f(0.5f), M(m, m));
	/* k*LH - (h - (s*(h+r) + k *LL) - m) */
	/* Arrange to k(LH - LL) + h(s-1) + rs - m */
	res = F(L(n2Hi), k, F(L(n2Lo), k, F(R, s, F(hfsq,s,S(m, hfsq)))));

	/* Handle zeros  */
#define KLD
#ifdef KLD
	res = vblendf(cmp, res, vset1f(0.f));
#else
	res = vblendf(cmp, res, vset1f(-INFINITY));
#endif
	return res;
#undef M
#undef A
#undef L
#undef F
}

