#include "matrix.h"
#include "util.h"
	
struct matrix *
matrix_new(struct matrix *result, size_t rows, size_t cols)
{
	size_t ld;
	float *data;

	/* Compute leading dimension */
	ld = padcount(rows, sizeof(*result->data));

	/* Allocate */
	data = (float *)acalloc(ld * cols, sizeof(*result->data));
	if (ld > 0 && data == NULL)
		return NULL;

	/* Store */
	result->data = data;
	result->ld = ld;
	result->rows = rows;
	result->cols = cols;

	return result;
}

void
matrix_center(struct matrix *m, float *mean)
{
	size_t i, j;
	for (i = 0; i < m->cols; i++) {
		mean[i] = 0.f;
		for (j = 0; j < m->rows; j++)
			mean[i] += m->data[i*m->ld + j];
	}

	for (i = 0; i < m->cols; i++)
		mean[i] /= (float) m->rows;

	for (i = 0; i < m->cols; i++)
		for (j = 0; j < m->rows; j++)
			m->data[i*m->ld + j] -= mean[i];
}

int
dump_smat_bin(struct matrix *m, FILE *fp)
{
	size_t i;
	i = 0;
	for (i = 0; i < m->cols; i++) {
		/* FIXME: check return value */
		(void)fwrite(&m->data[i*m->ld + i], sizeof(*m->data), m->cols-i, fp);
	}
	return 0;
}

struct matrix*
load_smat_bin(FILE *fp, struct matrix *m)
{
	size_t i, j;

	for (i = 0, j = 0; i < m->cols; i++) {
		/* Read column */
		if (fread(&m->data[i*m->ld + i], sizeof(*m->data), (m->cols-i), fp) != (m->cols-i))
			return NULL;
		/* Symmetrize */
		for (j = i; j < m->cols; j++) {
			m->data[j*m->ld+i] = m->data[i*m->ld + j];
		}
	}
	return m;
}
