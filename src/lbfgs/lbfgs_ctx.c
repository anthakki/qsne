#include <math.h>
#include <float.h>
#include "lbfgs_ctx.h"
#include "../util.h"


static float *copy_vec(int ndims, const float *src, float *vec)
{
	memcpy(vec, src, ndims * sizeof(float));
	return vec;
}

int lbfgs_ctx_init(struct lbfgs_ctx *lbfgs, const size_t hess_rank, const size_t ndims)
{
	/* Set everything to zero. */
	memset(lbfgs, 0, sizeof(*lbfgs));

	if (ring_buf_init(&lbfgs->grads, hess_rank, ndims) ||
		ring_buf_init(&lbfgs->points, hess_rank, ndims) ||
		(matrix_new(&lbfgs->search_dir, ndims , 4) == NULL) ||
		(matrix_new(&lbfgs->alpha, hess_rank, 1) == NULL))
		return -1;

	lbfgs->damp = -HUGE_VALF; /* No damping */
	lbfgs->hess_rank = hess_rank;
	lbfgs->ndims = ndims;
	return 0;

}

void lbfgs_ctx_deinit(struct lbfgs_ctx *lbfgs)
{
	if (&lbfgs->grads != NULL)
		ring_buf_delete(&lbfgs->grads);
	if (&lbfgs->points != NULL)
		ring_buf_delete(&lbfgs->points);
	if (lbfgs->search_dir.data != NULL)
		free(lbfgs->search_dir.data);
	if (lbfgs->alpha.data != NULL)
		free(lbfgs->alpha.data);
}

void lbfgs_ctx_iter(struct lbfgs_ctx *lbfgs, const float *point,
		    const float *grad, float *next_point)
{
	float *glast = ring_buf_get_off(&lbfgs->grads, 0, 1);
	float *xlast = ring_buf_get_off(&lbfgs->points, 0, 1);
	memcpy(glast, grad, lbfgs->ndims * sizeof(float));
	memcpy(xlast, point, lbfgs->ndims * sizeof(float));

	{{
	int i;

	/* Get latest point and gradient. */
	lbfgs_ctx_calc_search_dir(lbfgs);

	/* Plain gradient descent, but back out heavily */
	if (next_point != NULL)
	for (i = 0; i < lbfgs->ndims; ++i) {
#		define ALPHA (1)

		next_point[i] = point[i] + ALPHA * (-lbfgs->search_dir.data[i]);
	}
	}}
}

/* Damps the gradient difference such the problem remains better conditioned */
static void mod_gdiff(float *gdiff, const float *xdiff, const float *grad, const size_t dims, const float damp)
{
	float a, c;
	size_t i;

	/* Compute magnitudes */
	a = 0.f;
	for (i = 0; i < dims; ++i)
		a += gdiff[i] * xdiff[i]; /* y'*s */
	c = 0.f;
	for (i = 0; i < dims; ++i)
		c += xdiff[i] * -grad[i]; /* s'*B*s */

	/* Apply Powell damping */
	if (!(a >= damp * c))
		for (i = 0; i < dims; ++i)
			gdiff[i] = damp * -grad[i]; /* theta = (1-s)*c/(c-a) */
	else if (!(a <= 1.f/damp * c))
		for (i = 0; i < dims; ++i)
			gdiff[i] = 1.f/damp * -grad[i]; /* theta = (1+s)*c/(a-c) */
}

void calc_search_dir(float *grads, float *points, float *ctx, float *a, const size_t ndims, const size_t rb_head, const size_t rb_size, const float damp, const size_t ld)
{
	size_t cur_ind = 0;
	float b = 0;
	float a_num, a_denom;
	size_t i, j;
	size_t ix1, ix2;

	float *search_dir = ctx;
	float *grad_tmp = copy_vec(ndims, &grads[rb_ind_off(rb_size, rb_head, 0)*ld], &ctx[ld]);
	float *gdiff = &ctx[2*ld];
	float *xdiff = &ctx[3*ld];
	float *alpha = a;


	for (i = 1; i < rb_size; i++) {
		cur_ind = rb_size -1 -i;
		ix1 = rb_ind_off(rb_size, rb_head, -i + 1);
		ix2 = rb_ind_off(rb_size, rb_head, -i);

		vecdiff(ndims, &grads[ix1*ld],
			&grads[ix2*ld], gdiff);
		vecdiff(ndims, &points[ix1*ld],
			&points[ix2*ld], xdiff);

		if (damp >= 0.f)
			mod_gdiff(gdiff, xdiff, &grads[ix2*ld], ndims, damp);

		a_num = a_denom = 0.f;
		for (j = 0; j < ndims; ++j)
		{
			a_num += xdiff[j] * grad_tmp[j];
			a_denom += xdiff[j] * gdiff[j];
		}

		alpha[cur_ind] = a_num / a_denom;

		if (a_denom > FLT_MIN) /* take 0/0 -> 0 */
			for (j = 0; j < ndims; j++) {
				grad_tmp[j] -= alpha[cur_ind] * gdiff[j];
			}
	}


	/* assume scaled identity for initial (inverse) Hessian */ 
	{{
		float gamma;

		gamma = 1.f;
		if (rb_size > 1)
		{
			float aa, bb;
			ix1 = rb_ind_off(rb_size, rb_head, 0);
			ix2 = rb_ind_off(rb_size, rb_head, -1);
			assert(ix1 != ix2);

			vecdiff(ndims, &grads[ix1*ld],
				&grads[ix2*ld], gdiff);
			vecdiff(ndims, &points[ix1*ld],
				&points[ix2*ld], xdiff);

			if (damp >= 0.f)
				mod_gdiff(gdiff, xdiff, &grads[ix2*ld], ndims, damp);

			aa = bb = 0.f;
			for (j = 0; j < ndims; ++j)
			{
				aa += gdiff[j] * xdiff[j];
				bb += gdiff[j] * gdiff[j];
			}

			if (bb > FLT_MIN) /* take 0/0 -> 0 */
				gamma = aa / bb;
		}

		for (j = 0; j < ndims; ++j)
			search_dir[j] = gamma * grad_tmp[j];
	}}

	/* All saved pairs prior to this iteration. */
	{{
	int off = 0;
	for (i = 1; i < rb_size; i++) {
		float b_num = 0;
		float b_denom = 0;
		off = -rb_size + i;
		ix1 = rb_ind_off(rb_size, rb_head, off + 1);
		ix2 = rb_ind_off(rb_size, rb_head, off);
		assert(ix1 != ix2);

		vecdiff(ndims, &grads[ix1*ld],
			&grads[ix2*ld], gdiff);
		vecdiff(ndims, &points[ix1*ld],
			&points[ix2*ld], xdiff);

		if (damp >= 0.f)
			mod_gdiff(gdiff, xdiff, &grads[ix2*ld], ndims, damp);

		b_num = b_denom = 0.f;
		for (j = 0; j < ndims; ++j)
		{
			b_num += gdiff[j] * search_dir[j];
			b_denom += gdiff[j] * xdiff[j];
		}

		b = b_num / b_denom;

		if (b_denom > FLT_MIN) /* take 0/0 -> 0 */
			for (j = 0; j < ndims; ++j)
				search_dir[j] += (( alpha[i - 1] - b )) * xdiff[j];
	}
	}}
}

/* Calculate search direction = p_k = - H_k \grad_k */
void lbfgs_ctx_calc_search_dir(struct lbfgs_ctx *prob)
{

	assert(prob->search_dir.ld == prob->grads.m.ld);
	calc_search_dir(prob->grads.m.data, prob->points.m.data, prob->search_dir.data, prob->alpha.data, prob->ndims, prob->grads.head, prob->grads.size, prob->damp, prob->search_dir.ld);

}

void vecdiff(int ndims, const float *vec1, const float *vec2, float *res)
{
	int i;
	for (i = 0; i < ndims; i++)
		res[i] = vec1[i] - vec2[i];
}
