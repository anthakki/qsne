#ifndef RING_BUF_H
#define RING_BUF_H

#include <stdlib.h>
#include "../matrix.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Ring buffer for vectors of type float. */
struct ring_buf {
	int ndims;
	float *arr;
	size_t max_size, size, head;
	struct matrix m;
};

/* Increment ring buffer head */
void ring_buf_advance(struct ring_buf *rb);
/* Init ring buf with maximum size of max_size, each item containing ndims floats */
int ring_buf_init(struct ring_buf *rb, size_t max_size, size_t ndims);
void ring_buf_delete(struct ring_buf *rb);
/* Given a ring buffer of size cur_size and most recent index head, return the index obtained when offset (negative or positive) off is added to head. */
size_t rb_ind_off(size_t cur_size, size_t head, int off);
/* Get item relative to current head by offset off in ringbuffer. If advance > 0, current head is set to next item. */
float *ring_buf_get_off(struct ring_buf *rb, int offset, int advance);


#ifdef __cplusplus
}
#endif

#endif
