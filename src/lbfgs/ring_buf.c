#include "ring_buf.h"

int ring_buf_init(struct ring_buf *rb, size_t max_size, size_t ndims)
{
	if (matrix_new(&rb->m, ndims, max_size) == NULL)
		return -1;
	rb->ndims = ndims;
	rb->head = 0;
	rb->max_size = max_size;
	rb->size = 0;
	return 0;
}

void ring_buf_delete(struct ring_buf *rb)
{
	free(rb->m.data);
}

void ring_buf_advance(struct ring_buf *rb)
{
	int new_head;
	if (rb->size == 0) {
		rb->size += 1;
		return;
	}
	new_head = (rb->head + 1) % rb->max_size;
	if (rb->size < rb->max_size)
		rb->size += 1;
	rb->head = new_head;
}

size_t rb_ind_off(size_t cur_size, size_t head, int off)
{
	if (off < 0)
		return (cur_size-1) - (((cur_size-1) - head -(size_t) off)% cur_size);
	return (head + off) % cur_size;
}

float *ring_buf_get_off(struct ring_buf *rb, int offset, int advance)
{
	size_t ix;
	if (advance)
		ring_buf_advance(rb);
	ix = rb_ind_off(rb->size, rb->head, offset);
	return &(rb->m.data[ix * rb->m.ld]);
}
