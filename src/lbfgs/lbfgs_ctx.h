#ifndef LBFGS_CTX_H
#define LBFGS_CTX_H

#include "ring_buf.h"
#include <assert.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

struct lbfgs_ctx {
	size_t hess_rank;
	int iter;
	int ndims;
	struct ring_buf grads, points;
	/* Contains: search_dir, grad_tmp, gdiff, xdiff */
	struct matrix search_dir;
	struct matrix alpha;
	float damp;
};

/* Init lbfgs_ctx, with ring buffer of size mem_size and ndims number of points */
int lbfgs_ctx_init(struct lbfgs_ctx *lbfgs, const size_t hess_rank, const size_t ndims);
void lbfgs_ctx_deinit(struct lbfgs_ctx *lbfgs);
/* Add point and gradient to ringbuffer and get search direction */
void lbfgs_ctx_iter(struct lbfgs_ctx *lbfgs, const float *point,
		    const float *grad, float *next_point);
/* Substract two vectors of size ndims: res = vec1 - vec2*/
void vecdiff(int ndims, const float *vec1, const float *vec2, float *res);
/* Given ringbuffers for gradients and points, get search direction */
void calc_search_dir(float *grads, float *points, float *ctx, float *a, const size_t ndims, const size_t rb_head, const size_t rb_size, const float damp, const size_t ld);
/* Calculate search direction */
void lbfgs_ctx_calc_search_dir(struct lbfgs_ctx *prob);

#ifdef __cplusplus
}
#endif

#endif
