#ifndef TSVREADER_H_
#define TSVREADER_H_

/*
 * Parser for tab/space delimited text files with numeric content
 */

#include <stddef.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Context for the parser */
typedef struct tsvreader_ tsvreader_t;

/* Create a new parser */
void tsvreader_init(tsvreader_t *self);

/* Parse data */
int tsvreader_parse(tsvreader_t *self, FILE *file);

/* Get number of rows */
size_t tsvreader_rows(const tsvreader_t *self);
/* Get number of columns */
size_t tsvreader_cols(const tsvreader_t *self);

/* Get data */
const float *tsvreader_data(const tsvreader_t *self);

/* Free data */
void tsvreader_deinit(tsvreader_t *self);

/*- Guts--don't look */

struct tsvreader_ {
	size_t rows_, cols_;
	struct tsvreader_buffer_ {
		float *data_;
		size_t size_;
	} data_;
};

#define tsvreader_rows tsvreader_rows_
#define tsvreader_rows_(self) \
	((size_t)(self)->rows_)

#define tsvreader_cols tsvreader_cols_
#define tsvreader_cols_(self) \
	((size_t)(self)->cols_)

#define tsvreader_data tsvreader_data_
#define tsvreader_data_(self) \
	((const float *)(self)->data_.data_)

#ifdef __cplusplus
}
#endif

#endif /* TSVREADER_H_ */
