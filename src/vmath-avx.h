#ifndef VMATH_DEFINED
#define VMATH_DEFINED "Intel AVX"

/* Intel AVX math. Requires AVX support (Sandy Bridge and newer) */
/* See vmath-iso.h for the API details */

#include <immintrin.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef __m256 vfloat;
#define vcountf (8)

#define vzerof() (_mm256_setzero_ps())

#define vloadf(p) (_mm256_load_ps((p)))
#define vstoref(p, x) (_mm256_store_ps((p), (x)))

#define vset1f(x) (_mm256_set1_ps((x)))

static __inline float vget1f(__m256 __x) {
	/* Get the lowest word */
	return __x[0];
}

#define vaddf(x, y) (_mm256_add_ps((x), (y)))
#define vsubf(x, y) (_mm256_sub_ps((x), (y)))
#define vmulf(x, y) (_mm256_mul_ps((x), (y)))
#define vdivf(x, y) (_mm256_div_ps((x), (y)))

#define vrcpf(x) (_mm256_rcp_ps((x)))

/* FMA requires Haswell or newer */
#if defined(__FMA__) || (!defined(__FMA__) && defined(__AVX2__))
#	define vfmaddf(a, b, c) (_mm256_fmadd_ps((a), (b), (c)))
#else
#	define vfmaddf(a, b, c) (vaddf(vmulf((a), (b)), (c)))
#endif

static __inline __m256 vexpf(__m256 __x) {
	/* Implement exp() using the math library-- slow but it's not critical for us */
	__m256 __y;
	int __i;
	for (__i = 0; __i < vcountf; ++__i)
		__y[__i] = expf(__x[__i]);
	return __y;
}

static __inline __m256 vlogf(__m256 __x) {
	/* Implement log() using the math library-- slow but it's not critical for us */
	__m256 __y;
	int __i;
	for (__i = 0; __i < vcountf; ++__i)
		__y[__i] = logf(__x[__i]);
	return __y;
}

static __inline __m256 vhaddf(__m256 __x) {
	/* Permute data across lanes */
	__m256 __y = _mm256_permute2f128_ps(__x, __x, 0x01); /* { e,f,g,h, a,b,c,d } */
	/* Add */
	__m256 __t = _mm256_hadd_ps(__x, __y); /* { ab,cd,ef,gh, ef,gh,ab,cd } */
	__t = _mm256_hadd_ps(__t, __t); /* { abcd,efgh,abcd,efgh, efgh,abcd,efhg,abcd } */
	__t = _mm256_hadd_ps(__t, __t);
	return __t;
}

#define vblendf(x, y, z) (_mm256_blendv_ps((y), (z), (x)))

typedef __m256i vint;
#define vset1i(x) (_mm256_set1_epi32(x))
#define vsubi(x, y) (_mm256_sub_epi32((x), (y)))
#define cvtepii(x) (_mm256_cvtepi32_ps(x))
#define vsraii(x, imm) (_mm256_srai_epi32(x, imm))

#ifdef __cplusplus
}
#endif 

#endif /* VMATH_DEFINED */
