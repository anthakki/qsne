#ifndef MATRIX_H_
#define MATRIX_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include "math.h"

struct matrix {
	/* Data, must be allocated with QSNE to have proper alignment */
	float *data;
	/* Leading dimension (must be padded for QNSE), rows, columns */
	size_t ld, rows, cols;
};

/* Create new matrix */
struct matrix* matrix_new(struct matrix *result, size_t rows, size_t cols);
/* Center a matrix -- compute and substract column-wise means. */
void matrix_center(struct matrix *m, float *mean);
/* Save symmetric matrix to file in binary format */
int dump_smat_bin(struct matrix *m, FILE *fp);
/* Load symmetric matrix from file (binary format) */
struct matrix* load_smat_bin(FILE *fp, struct matrix *m);

#ifdef __cplusplus
}
#endif

#endif /* MATRIX_H_ */
