
#include "pca.h"
#include <assert.h>
#include <string.h>

/* Signatures for LAPACK Fortran routines */

void ssytrd_(const char *uplo, const ptrdiff_t *n, float *a,
	const ptrdiff_t *lda, float *d, float *e, float *tau, float *work,
	const ptrdiff_t *lwork, ptrdiff_t *info);

void sorgtr_(const char *uplo, const ptrdiff_t *n, float *a,
	const ptrdiff_t *lda, const float *tau, float *work,
	const ptrdiff_t *lwork, ptrdiff_t *info);

void ssteqr_(const char *compz, const ptrdiff_t *n, float *d, float *e,
	float *z, const ptrdiff_t *ldz, float *work, ptrdiff_t *info);

static
void
rcov(float *C, const float *x, size_t ld, size_t m, size_t n)
	/* Find unscaled covariance matrix */
{
	size_t j, i, j1, j2;
	float sum, mu, nu;

	assert(C != NULL || n < 1);
	assert(x != NULL || m*n < 1);

	/* Find column means */
	for (j = 0; j < n; ++j)
	{
		/* Compute sum */
		sum = 0.;
		for (i = 0; i < m; ++i)
			sum += x[i + j*ld];

		/* Store mean */
		C[j + j*ld] = sum / m;
	}

	/* Find second moments */
	for (j1 = 0; j1 < n; ++j1)
	{
		/* Get mean */
		mu = C[j1 + j1*ld];

		/* Go through the other variables */
		for (j2 = j1; j2 < n; ++j2)
		{
			/* Get mean */
			nu = C[j2 + j2*ld];

			/* Compute sum */
			sum = 0.;
			for (i = 0; i < m; ++i)
				sum += (x[i + j1*ld] - mu) * (x[i + j2*ld] - nu);

			/* Store squared error */
			C[j1 + j2*n] = sum;
		}

		/* Fill in the upper triangle */
		for (j2 = j1+1; j2 < n; ++j2)
			C[j2 + j1*n] = C[j1 + j2*n];
	}
}

void
pca(float *coefs, float *covs, const float *data, size_t ldda, size_t samps, size_t dims)
{
	assert(coefs != NULL || dims < 1);
	assert(covs != NULL || dims < 1);
	assert(data != NULL || samps*dims < 1);

	/* Find covariance matrix */
	rcov(covs, data, ldda, samps, dims);

	/* Do PCA */
	pcacov(coefs, covs, dims);
}

static
void
fliplr(float *x, size_t ld, size_t m, size_t n)
	/* Flip the columns of a matrix in place */
{
	size_t j, i;
	double tmp;

	/* Check for an empty problem */
	if (!(n > 1))
		return;

	/* Swap columns */
	for (j = 0; j < n-(j+1); ++j)
		for (i = 0; i < m; ++i)
		{
			tmp = x[i + j*ld];
			x[i + j*ld] = x[i + (n-(j+1))*ld];
			x[i + (n-(j+1))*ld] = tmp;
		}
}

static
void
makediag(float *D, float *d, size_t ld, size_t n)
	/* Expand a vector into a diagonal matrix (possibly in place) */
{
	size_t j, i;

	/* NB. careful here, as D and d might overlap */

	/* Move in the values */
	memmove(D, d, n * sizeof(*d));

	/* Permute & fill */
	for (j = n; j-- > 0;)
	{
		/* Subdiagonal */
		for (i = n; --i > j;)
			D[i + j*ld] = 0.;

		/* Diagonal */
			D[j + j*ld] = D[j];

		/* Superdigonal */
		for (i = j; i-- > 0;)
			D[i + j*ld] = 0.;
	}
}

void
pcacov(float *coefs, float *covs, size_t dims)
{
	ptrdiff_t n, lwork, info;
#	define SMALL (32)   /* NB. >= 7 */
	float buffer[SMALL * (3 + SMALL)], *scratch;
	char mode;

	assert(coefs != NULL || dims < 1);
	assert(covs != NULL || dims < 1);

	/* Set up constants */ 
	n = (ptrdiff_t)dims;

	/* Set up scratch-- at least 4*n slots are needed, up to n^2 can be useful */
	scratch = buffer;
	lwork = SMALL * SMALL;
	if (n > SMALL)
	{
		scratch = covs;
		lwork = (n-3) * n;
	}

	/* Make Hesseberg form */
	memcpy(coefs, covs, n*n * sizeof(*covs));
	mode = 'U';
	ssytrd_(&mode, &n, coefs, &n, &scratch[0*n], &scratch[1*n], &scratch[2*n],
		&scratch[3*n], &lwork, &info);

	/* Get the transform */
	mode = 'U';
	sorgtr_(&mode, &n, coefs, &n, &scratch[2*n], &scratch[3*n], &lwork, &info);

	/* Diagonalize the Hessenberg form */
	mode = 'V';
	ssteqr_(&mode, &n, &scratch[0*n], &scratch[1*n], coefs, &n,
		&scratch[3*n], &info);

	/* Sort eigenvalues-- LAPACK gives in ascending order */
	fliplr(coefs, dims, dims, dims);
	fliplr(&scratch[0*n], 1, 1, dims);
	makediag(covs, &scratch[0*n], dims, dims);
}
