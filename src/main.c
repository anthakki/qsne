#include "lbfgs/lbfgs_ctx.h"
#ifdef _OPENMP
#include <omp.h>
#endif
#include "pca.h"
#include "qsne.h"
#include "rgetopt.h"
#include "tsvreader.h"
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <unistd.h>
#include "util.h"
#include "matrix.h"

/* Macro definitions */
#define SFPRINTF(fp, ...) do { if (fp != NULL) fprintf(fp, __VA_ARGS__); } while(0)

#ifdef VERBOSE
#define DPRINTF(...) do { fprintf(stderr,  __VA_ARGS__); } while(0)
#else
#define DPRINTF(...) do { } while(0)
#endif

/* Constants */
#define GDM_ALPHA_A     (.5f)          /* Initial momentum */
#define GDM_ALPHA_B     (.8f)          /* Momentum after switch */
#define GDM_ETA         (200.f * .25f) /* Learning rate. NOTE: t-SNE lacks the scale of 4 */
#define GDM_MSWITCH_I   (250u)         /* At which iteration to switch momentum and stop exaggerating P */
#define PSCALE_FACTOR   (4.f)          /* P-matrix exaggeration factor */
#define MIN_ITERS       (15u)          /* Used to prevent early exit if optimization starts sluggishly */

#ifdef TIMER

struct timer {
	double beg, end, acc;
	unsigned do_dump;
	const char *id;
};

static struct timer g_all_timers[] = {
	{0., 0., 0., 1, "optimization"},
	{0., 0., 0., 1, "kld_computation"},
	{0., 0., 0., 1, "indist_computation"},
	{0., 0., 0., 1, "entropy_computation"},
	{0., 0., 0., 0, "tmp"},
	{0., 0., 0., 1, "all"}
};

struct timer*
get_timer(struct timer *tt, const char *id, size_t len) {
	size_t i;
	for (i = 0; i < len; i++)
		if (!strcmp(tt[i].id, id))
			return &tt[i];
	return NULL;
}

double
timeval2sec(struct timeval t) {
	return (double)t.tv_sec + 1e-6*(double)t.tv_usec;
}

/* Start timer. Duration as seconds . */
void
timer_start(struct timer* t) {
	struct rusage r;
	t->beg = t->end = 0.;
	t->acc = 0.;
	getrusage(RUSAGE_SELF, &r);
	t->beg = timeval2sec(r.ru_utime) + timeval2sec(r.ru_stime);
}

void
timer_end(struct timer *t) {
	struct rusage r;
	getrusage(RUSAGE_SELF, &r);
	t->end = timeval2sec(r.ru_utime) + timeval2sec(r.ru_stime);
	/* Calculate diff*/
	t->acc = t->end - t->beg;
}

double
timer_finish(struct timer *t) {
	timer_end(t);
	return t->acc;
}

#define GET_TIMER(x) get_timer(g_all_timers, x, sizeof(g_all_timers) / sizeof(*g_all_timers))
#define TIMER_START(x) timer_start(GET_TIMER(x))
#define TIMER_END(x) timer_end(GET_TIMER(x))
#define TIMER_FINISH(x) fprintf(stderr, "cpu_time_%s (s): %g\n", x, timer_finish(GET_TIMER(x)));

#else /* TIMER */

struct timer_stub {
	double acc;
};
static struct timer_stub g_timer_stub;

#define GET_TIMER(x) (&g_timer_stub)

#define TIMER_START(x) ((void)0)
#define TIMER_END(x) ((void)0)
#define TIMER_FINISH(x) ((void)0)

#endif /* TIMER */

union compat_opts {
	unsigned x;
	struct {
		unsigned SCALE : 1;      /* Scale the input to the incorrect space */
		unsigned MOMENTUM : 1;   /* Apply momentum to restrict updates */
		unsigned SWITCH : 1;     /* Switch parameters after initial transient */
		unsigned CENTER : 1;     /* Center data on each round */
	} o;
};

#define IN_FORMAT_RAW (0)   /* Raw data, distances will be computed */
#define IN_FORMAT_SQEUC (1) /* Squared euclidean distances */
#define IN_FORMAT_EUC (2)   /* Euclidean distances */

struct opts {
	/* Dimension of the output */
	size_t outcols;
	/* Perplexity */
	float perp_lo, perp_hi;

	/* Maximum number of iterations */
	size_t max_iters;
	/* Tolerace for stopping condition */
	float min_delta;
	/* Number of worker threads */
	int num_workers;
	/* Maximum rank of Hessian in quadratic approximation */
	size_t hess_rank;
	/* Compatibility flags with T-SNE */
	union compat_opts compat_opts;
	/* Line search parameters: slope factor, backout length */
	float ls_coef, ls_step;
	/* Powell damping parameter */
	float bfgs_damp;

	/* Initial condition file */
	const char *init_fn;
	/* Output file */
	const char *out_fn;
	/* Objective function file. */
	const char *kld_fn;
	/* Entropy estimates file. */
	const char *ent_fn;
	/* Perplexity tuning results. */
	const char *ptune_fn;
	/* Y values */
	const char *y_dir_fn;
	/* P matrix */
	const char *p_fn;
	/* Input format */
	int in_format;
	/* How often to dump the objective and Y values */
	int kld_dump_interval;
	/* Rtsne compatibility related options */
	unsigned long mom_switch_iter;

}; /* struct opts */

/* Returns 0 if file is not writable. */
static
int
is_writable(const char *path)
{
	FILE *fp;
	int r, w;
	r = ((fp = fopen(path, "r")) != NULL);
	w = ((r ? fclose(fp) : 0), ((fp = fopen(path, "a")) != NULL));
	if (w) {
		fclose(fp);
		if (!r)
			unlink(path);
	}
	return w;
}

static
const char*
verify_outs(struct opts* o) {
#define IW(id) if ((id != NULL) && !is_writable(id)) { \
	fprintf(stderr, "Path %s is not writable", id); return id; }
	IW(o->out_fn);
	IW(o->kld_fn);
	IW(o->ent_fn);
	IW(o->ptune_fn);
	IW(o->y_dir_fn);
	return NULL;
#undef IW
}

static
void
default_opts(struct opts *opts)
{
	/* Default to 2-D */
	opts->outcols = 2;
	/* Perplexity */
	opts->perp_lo = 30.f;
	opts->perp_hi = opts->perp_lo;

	/* Iteration parameters */
	opts->max_iters = 1000;
	opts->min_delta = 1e-4f;
	opts->num_workers = -1;
	/* Optimizer parameters */
	opts->hess_rank = 10u;
	opts->compat_opts.x = 0u;
	opts->ls_coef = opts->ls_step = .5f;
	opts->bfgs_damp = -HUGE_VALF; /* NB. Disabled, typical value would be .2f */

	/* Optional file names */
	opts->init_fn = NULL;
	opts->out_fn = NULL;
	opts->kld_fn = NULL;
	opts->ent_fn = NULL;
	opts->ptune_fn = NULL;
	opts->y_dir_fn = NULL;
	opts->p_fn = NULL;

	/* Input data format */
	opts->in_format = IN_FORMAT_RAW;
	/* Objective function dump interval */
	opts->kld_dump_interval = 50;
	/* Rtsne compatibility related options */
	opts->mom_switch_iter = GDM_MSWITCH_I;
}

#ifdef VERBOSE

static
void
enabled_flags(union compat_opts compat_opts) {
	fprintf(stderr, "Enabled compatibility options:\n");
	if (compat_opts.o.SCALE)
		fprintf(stderr, "* %s\n", "Scale the input to the incorrect space");
	if (compat_opts.o.MOMENTUM)
		fprintf(stderr, "* %s\n", "Apply momentum to restrict updates");
	if (compat_opts.o.SWITCH)
		fprintf(stderr, "* %s\n", "Switch parameters after initial transient");
	if (compat_opts.o.CENTER)
		fprintf(stderr, "* %s\n", "Center data on each round");
}

#endif /* VERBOSE */

/* Forward declaration. */
static void dump_ent_and_ptune(const float*, const float*, const char*, const char*, size_t);

static
int
parse_opts(struct opts *opts, int argc, char **argv)
{
	rgetopt_t getopt;
	int result;
	unsigned long lu;
	float f, ff;
	char ch;

	/* Parse options */
	
	for (rgetopt_init(&getopt, argc, argv, "d:p: k:t:m: C L:D: f: T: i:o: O:P:S:Y:N: K:");
			(result = rgetopt_next(&getopt)) != -1;)
		switch (result)
		{
			/* Output dimension */
			case 'd':
				if (sscanf(getopt.optarg, "%lu%c", &lu, &ch) != 1)
					goto invalid_optarg;
				opts->outcols = lu;
				break;

			/* Perplexity */
			case 'p':
				if (sscanf(getopt.optarg, "%f:%f%c", &f, &ff, &ch) == 2)
				{
					opts->perp_lo = f;
					opts->perp_hi = ff;
				}
				else if (sscanf(getopt.optarg, "%f%c", &f, &ch) == 1)
					opts->perp_lo = opts->perp_hi = f;
				else
					goto invalid_optarg;
				break;

			/* Maximum number of iterations */
			case 'k':
				if (sscanf(getopt.optarg, "%lu%c", &lu, &ch) != 1)
					goto invalid_optarg;
				opts->max_iters = lu;
				break;

			/* Tolerance for stopping condition */
			case 't':
				if (sscanf(getopt.optarg, "%f%c", &f, &ch) != 1)
					goto invalid_optarg;
				opts->min_delta = f;
				break;

			/* Maximum rank for Hessian approximation */
			case 'm':
				if (sscanf(getopt.optarg, "%lu%c", &lu, &ch) != 1)
					goto invalid_optarg;
				opts->hess_rank = lu;
				break;

			/* Rtsne compatibility mode */
			case 'C':
				opts->compat_opts.x = (unsigned)-1;
				break;

			/* Line search parameters */
			case 'L':
				if (sscanf(getopt.optarg, "%f,%f%c", &f, &ff, &ch) == 2)
					opts->ls_coef = f, opts->ls_step = ff;
				else if (sscanf(getopt.optarg, "%f%c", &f, &ch) == 1)
					opts->ls_coef = opts->ls_step = f;
				else if (strcmp(getopt.optarg, "off") == 0)
					opts->ls_coef = opts->ls_step = -HUGE_VALF;
				else
					goto invalid_optarg;
				break;

			/* Input format flags */
			case 'f':
				if (strcmp(getopt.optarg, "raw") == 0)
					opts->in_format = IN_FORMAT_RAW;
				else if (strcmp(getopt.optarg, "squaredeuclidean") == 0)
					opts->in_format = IN_FORMAT_SQEUC;
				else if (strcmp(getopt.optarg, "euclidean") == 0)
					opts->in_format = IN_FORMAT_EUC;
				else
					goto invalid_optarg;
				break;

			/* Powell damping */
			case 'D':
				if (sscanf(getopt.optarg, "%f%c", &f, &ch) == 1)
					opts->bfgs_damp = f;
				else
					goto invalid_optarg;
				break;

			/* Worker thread count */
			case 'T':
				if (sscanf(getopt.optarg, "%lu%c", &lu, &ch) != 1)
					goto invalid_optarg;
				opts->num_workers = lu;
				break;

			/* Initial condition file */
			case 'i':
				opts->init_fn = getopt.optarg;
				break;

			/* Output file */
			case 'o':
				opts->out_fn = getopt.optarg;
				break;

			/* Objective function dump path */
			case 'O':
				opts->kld_fn = getopt.optarg;
				break;

			/* Perp. tune result dump path */
			case 'P':
				opts->ptune_fn = getopt.optarg;
				break;

			/* Entropy estimates dump path */
			case 'S':
				opts->ent_fn = getopt.optarg;
				break;

			/* Prefix for Y-values path. (full path: pfx_y_iter.tsv) */
			case 'Y':
				opts->y_dir_fn = getopt.optarg;
				break;

			/* P-matrix path */
			case 'N':
				opts->p_fn = getopt.optarg;
				break;

			/* Objective function dump interval. */
			case 'K':
				if (sscanf(getopt.optarg, "%lu%c", &lu, &ch) != 1)
					goto invalid_optarg;
				if (lu > 0)
					opts->kld_dump_interval = lu;
				else
					goto invalid_optarg;
				break;

			/* Invalid option */
			default: /* '?' */
				fprintf(stderr, "%s: invalid option -%c" "\n",
					argv[0], getopt.optopt);
				return -1;

			/* Invalid option argument */
			invalid_optarg:
				fprintf(stderr, "%s: invalid argument \"%s\" for -%c" "\n",
					argv[0], getopt.optarg, getopt.optopt);
				return -1;
		}

	return getopt.optind;
}

static
void
usage(const char *argv0)
{
	fprintf(stderr,
"Usage: %s [options] input.tsv"                     "\n"
"Options:"                                          "\n"
"  -d dims  -k iters  -C       -T thr  -f fmt  -i init.tsv" "\n"
"  -p perp  -t tol    -L ls                    -o out.tsv " "\n"
"           -m rank   -D damp                             " "\n"
#if VERBOSE
"           -K oival                           -O obj.tsv " "\n"
"                                              -P ptn.tsv " "\n"
"                                              -S ent.tsv " "\n"
"                                              -Y pfx_yyy " "\n"
"                                              -N pfn.bin " "\n"
#endif
		, argv0);
}

struct cupstack {
	/* A stack of cleanup functions */
	struct cupstack_node { 
		void (*callback)(void *);
		void *cookie;
	} nodes[32];

	/* Stack pointer */
	size_t top;

};

static
void
cupstack_init(struct cupstack *self)
{
	/* Reset counter */
	self->top = 0;
}

static
void
cupstack_push(struct cupstack *self, void (*callback)(void *), void *cookie)
{
	/* Store pair */
	self->nodes[self->top].callback = callback;
	self->nodes[self->top].cookie = cookie;

	/* Bump counter */
	++self->top;
}

static
void
cupstack_swap(struct cupstack *self)
{
	struct cupstack_node tmp;

	/* Swap two topmost elements */ 
	tmp = self->nodes[self->top - 2];
	self->nodes[self->top - 2] = self->nodes[self->top - 1];
	self->nodes[self->top - 1] = tmp;
}

static
void
cupstack_pop(struct cupstack *self)
{
	/* Update counter */
	--self->top;
	/* Invoke handler */
	(*self->nodes[self->top].callback)(self->nodes[self->top].cookie);
}

static
void
cupstack_deinit(struct cupstack *self)
{
	/* Free data */
	while (self->top > 0)
		cupstack_pop(self);
}


static
const char *
read_tsv(struct matrix *result, const char *filename)
{
	FILE *file;
	tsvreader_t reader;
	size_t i, j;

	/* Open file */
	file = fopen(filename, "r");
	if (file == NULL)
		return "failed to open for read";

	/* Parse */
	tsvreader_init(&reader);
	if (tsvreader_parse(&reader, file) != 0)
	{
		tsvreader_deinit(&reader);
		fclose(file);
		return "invalid file format";
	}

	/* Close file */
	fclose(file);

	/* Allocate data */
	if (matrix_new(result, tsvreader_rows(&reader), tsvreader_cols(&reader)) == NULL)
	{
		tsvreader_deinit(&reader);
		return "no memory";
	}

	/* Copy & transpose */
	for (i = 0; i < result->rows; ++i)
		for (j = 0; j < result->cols; ++j)
			result->data[i + j * result->ld] = tsvreader_data(&reader)[i * result->cols + j];

	/* Free data */
	tsvreader_deinit(&reader);

	return NULL;
}

struct init_pca_cookie {
	const struct matrix *input, *covs, *output;
	float *centers, *coefs;
	size_t coefs_ld;
};

static
void
compute_mean_col_p(size_t id, const struct init_pca_cookie *cookie)
{
	/* Get center */
	cookie->centers[id] = qsne_mean(&cookie->input->data[id * cookie->input->ld],
		cookie->input->rows);
}

static
void
compute_cov_col_p(size_t id, const struct init_pca_cookie *cookie) 
{
	/* Get covariance */
	qsne_cov_part(cookie->covs->data, cookie->covs->ld, cookie->centers,
		cookie->input->data, cookie->input->ld, cookie->input->rows,
			cookie->input->cols, id, 1);
}

static
void
compute_score_col_p(size_t id, const struct init_pca_cookie *cookie)
{
	float scale;

	/* Factor in a scale to standardize the output */
	scale = sqrtf( (float)( cookie->input->rows - 1 ) /
		cookie->covs->data[id + id * cookie->covs->ld] );

	/* Get transformed coordinates */
	qsne_affine_part(&cookie->output->data[id * cookie->output->ld],
		cookie->output->ld, cookie->output->rows, 1,
			cookie->input->data, cookie->input->ld, cookie->input->cols,
				&cookie->coefs[id * cookie->coefs_ld], cookie->coefs_ld, cookie->centers, scale);
}

static
const char *
init_pca(struct matrix *result, size_t dims, const struct matrix *input)
{
	size_t ld;
	struct matrix scratch;
	struct init_pca_cookie cookie;

	/* Compute padded dimension */
	ld = padcount(input->cols, sizeof(float));

	/* Allocate data */
	if (matrix_new(result, input->rows, dims) == NULL)
		return "no memory";
	if (matrix_new(&scratch, ld, ld + ld + 1) == NULL)
	{
		free(result->data);
		return "no memory";
	}

	/* Fill in the cookie */
	cookie.input = input;
	cookie.covs = &scratch;
	cookie.centers = &scratch.data[2*ld * scratch.ld];
	cookie.coefs = &scratch.data[ld * scratch.ld];
	cookie.coefs_ld = ld;
	cookie.output = result;

	/* Compute input covariance */
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < input->cols; ++id)
		compute_mean_col_p(id, &cookie);
	}}
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < input->cols; ++id)
		compute_cov_col_p(id, &cookie);
	}}

	/* Get PCA coefficients */
	pcacov(cookie.coefs, cookie.covs->data, ld);

	/* Get scores */
	{{ size_t nz_dims, id;
	nz_dims = dims;
	if (dims > input->cols) /* Clamp dimension, rest are left zero */
		nz_dims = input->cols;
#pragma omp parallel for schedule(static)
	for (id = 0; id < nz_dims; ++id)
		compute_score_col_p(id, &cookie);
	}}

	return NULL;
}

/* This gradient descent with momentum update encloses all the T-SNE "magic"
 * coefficients and tricks */
enum opt_mode {
	NONE = 0,
	GDM,
	LBFGS
};

struct gdm_ctx {
	/* Total amount of data (rows*cols) */
	size_t elems;
	float *deltas, *gains;
	union compat_opts compat_opts;
	struct lbfgs_ctx lbfgs_ctx;
	float ls_coef, ls_step;
	float ls_obj, ls_slope;
	size_t iteration, hess_rank;
	size_t stop_lying_iter;
	float alpha_a, alpha_b, pscale, eta;
	struct matrix mean;
	enum opt_mode opt_mode;
};

static
int
gdm_init(struct gdm_ctx *self, size_t elems, const struct opts *opts)
{
	float *data;
	size_t i;

	/* Allocate memory */
	data = NULL;
	if (elems > 0 && (data = calloc(2 * elems, sizeof(*data))) == NULL)
		return -1;

	/* Set up the context */
	self->elems = elems;
	self->deltas = &data[elems];
	self->gains = &data[0];
	self->iteration = 0;
	self->hess_rank = opts->hess_rank;
	self->stop_lying_iter = GDM_MSWITCH_I;
	self->alpha_a = GDM_ALPHA_A;
	self->alpha_b = GDM_ALPHA_B;

	/* If this is <= 1.f, no exaggeration is done */
	self->pscale = 0.f;

	/* Scale if init not supplied */
	if ((opts->compat_opts.o.SCALE) && (opts->init_fn == NULL))
		self->pscale = PSCALE_FACTOR;

	/* Put in unity gains */
	for (i = 0; i < elems; ++i)
		self->gains[i] = 1.f;

	/* Store flags */
	self->compat_opts.x = opts->compat_opts.x;

	/* Copy line search parameters */
	self->ls_coef = opts->ls_coef;
	self->ls_step = opts->ls_step;

	/* Set up line search state */
	self->ls_obj = HUGE_VALF;
	self->ls_slope = 0.f;

	if (self->compat_opts.x) {
		if (lbfgs_ctx_init(&self->lbfgs_ctx, 1u, elems))
			return -1;
		self->opt_mode = GDM;
	}
	else {
		if (lbfgs_ctx_init(&self->lbfgs_ctx, opts->hess_rank + 1u, elems))
			return -1;
		self->opt_mode = LBFGS;
	}
	self->lbfgs_ctx.damp = opts->bfgs_damp;
	self->mean.data = NULL;

	return 0;
}

static
void
gdm_update(const struct gdm_ctx *self, const float *poss, const float *grad)
{
	size_t i;

	/* Apply BFGS */
	lbfgs_ctx_iter((struct lbfgs_ctx *)&self->lbfgs_ctx, poss, grad, NULL);
	grad = self->lbfgs_ctx.search_dir.data;

	/* Compute update */
	if (self->compat_opts.o.MOMENTUM != 0)
	{
		float alpha;
		/* Update gains */
		for (i = 0; i < self->elems; ++i)
		{
			/* Update gain */
			if (!(!(grad[i] * self->deltas[i] > 0.f))) /* NOTE: take for 0.f*0.f */
				self->gains[i] *= .8f;
			else
				self->gains[i] += .2f;

			/* Gain too small */
			if (self->gains[i] < .01f)
				self->gains[i] = .01f;
		}

		alpha = self->alpha_a;
		if (self->iteration >= self->stop_lying_iter)
			alpha = self->alpha_b;

		/* Compute deltas & apply update */
		for (i = 0; i < self->elems; ++i)
			self->deltas[i] = alpha * self->deltas[i] - GDM_ETA * self->gains[i] * grad[i];
	}
	else {
		/* Apply plain gradient */
		for (i = 0; i < self->elems; ++i)
			self->deltas[i] = -grad[i];
	}
}

static
void
gdm_deinit(struct gdm_ctx *self)
{
	/* Free memory */
	free(self->gains);
	if (self->mean.data != NULL)
		free(self->mean.data);
	lbfgs_ctx_deinit(&self->lbfgs_ctx);
}

struct make_dist_cookie {
	const struct matrix *dist;
	const struct matrix *poss;
	float *scratch;
	/* Entropies of input distributions. */
	float *ent;
	/* Perplexity tuning result. 1st column = original, 2nd = tuned. */
	float *ptune;
	float parms[2];
	/* Input format */
	int in_format;
};

static
void
inject_sqeuc_col(float *d, size_t ldd, const float *x, size_t ldx, size_t n, size_t j)
{
	size_t i;

#pragma omp simd
	for (i = 0; i < n; ++i)
		d[i + j*ldd] = x[i + j*ldx];
}

static
void
inject_euc_col(float *d, size_t ldd, const float *x, size_t ldx, size_t n, size_t j)
{
	size_t i;

#pragma omp simd
	for (i = 0; i < n; ++i)
		d[i + j*ldd] = x[i + j*ldx] * x[i + j*ldx];
}

static
void
make_normal_col_p(size_t id, struct make_dist_cookie *cookie)
{
	const struct matrix *dist, *poss;
	float perp_lo, perp_hi, *sum_dists, sigma;
	float ent;
	size_t j, i;

	/* Extract arguments */
	dist = cookie->dist;
	poss = cookie->poss;
	perp_lo = cookie->parms[0]; perp_hi = cookie->parms[1];
	sum_dists = cookie->scratch;
	
	/* Get column index */
	j = id;

	/* Find distances */
	switch (cookie->in_format)
	{
		case IN_FORMAT_RAW:
			qsne_pdist_part(dist->data, dist->ld, poss->data, poss->ld, poss->rows, poss->cols, j, 1u);
			break;

		case IN_FORMAT_SQEUC:
			inject_sqeuc_col(dist->data, dist->ld, poss->data, poss->ld, poss->cols, j);
			break;

		case IN_FORMAT_EUC:
			inject_euc_col(dist->data, dist->ld, poss->data, poss->ld, poss->cols, j);
			break;
	}

	/* Put Inf on the diagonal to ignore self */
	dist->data[j + j * dist->ld] = (float)HUGE_VAL;
	/* Put Inf on the padding */
	for (i = dist->rows; i < dist->ld; ++i)
		dist->data[i + j * dist->ld] = (float)HUGE_VAL;

	/* Find standard deviation */
	if (perp_lo < perp_hi)
		sigma = qsne_sigma_tune(&dist->data[j * dist->ld], logf(perp_lo), logf(perp_hi), dist->ld);
	else
		sigma = qsne_sigma(&dist->data[j * dist->ld], logf(perp_lo), dist->ld);

	/* Compute entropy if dumping is enabled. */
	if (cookie->ptune != NULL) {
		ent = qsne_normal_ent(&dist->data[j* dist->ld], -.5f/(sigma*sigma), dist->ld);
		cookie->ptune[id] = (perp_hi - perp_lo)/2.f + perp_lo;
		cookie->ptune[id + poss->rows] = expf(ent);
	}

	/* Get normal density */
	sum_dists[j] = qsne_normal(&dist->data[j * dist->ld], &dist->data[j * dist->ld], sigma, dist->ld);

	/* NOTE: T-SNE normalizes here as well, appropriate? */
	qsne_scale(&dist->data[j * dist->ld], 1.f / sum_dists[j], dist->ld);
	sum_dists[j] = 1.f;
}

static
void
scale_normal_col_p(size_t id, struct make_dist_cookie *cookie)
{
	const struct matrix *dist;
	float sum_dist;
	size_t j;

	/* Extract arguments */
	dist = cookie->dist;
	sum_dist = cookie->parms[0];
	/* Get column index */
	j = id;

	/* Normalize & symmetrize */
	qsne_symmetrize_part(dist->data, dist->ld, dist->rows, j, 1u, 1.f / sum_dist);
}

static
void
compute_ent_p(size_t id, struct make_dist_cookie *cookie)
{
	const struct matrix *dist;
	size_t j;

	/* Extract arguments */
	dist = cookie->dist;
	/* Get column index */
	j = id;

	/* Compute entropy */
	cookie->ent[j] = qsne_ent(&dist->data[j * dist->ld], dist->ld, dist->rows, 1u);
}

static
void
make_normal(float *scratch, const struct matrix *dist, const struct matrix *poss, float perp_lo, float perp_hi, const char *epath, const char *ppath, int in_format)
{
	struct make_dist_cookie cookie;
	float sum_dist;
	size_t j;

	/* Create cookie */
	cookie.dist = dist;
	cookie.poss = poss;
	cookie.scratch = scratch;
	cookie.ptune = cookie.ent = NULL;
	if (ppath != NULL)
		cookie.ptune = malloc(2*poss->rows*(sizeof(cookie.ptune)));

	if (epath != NULL || ppath != NULL)
		cookie.ent = malloc(poss->rows*(sizeof(cookie.ent)));

	cookie.in_format = in_format;

	/* Compute normal densities */
	cookie.parms[0] = perp_lo; cookie.parms[1] = perp_hi;
	DPRINTF("Computing normal densities\n");
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < poss->rows; ++id)
		make_normal_col_p(id, &cookie);
	}}

	/* Compute normalizer */
	sum_dist = 0.;
	for (j = 0; j < poss->rows; ++j)
		sum_dist += scratch[j];

	/* Scale & symmetrize-- NOTE: we need a sync to find the full scale */
	cookie.parms[0] = sum_dist;
	DPRINTF("Scaling & symmetrizing\n");
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < poss->rows; ++id)
		scale_normal_col_p(id, &cookie);
	}}

	/* Compute entropy */
	if (cookie.ent != NULL) {
		DPRINTF("Computing entropy...");
		TIMER_START("entropy_computation");
		{{ size_t id;
#pragma omp parallel for schedule(static)
		for (id = 0; id < poss->rows; ++id)
			compute_ent_p(id, &cookie);
		}}
		DPRINTF("done\n");
		TIMER_FINISH("entropy_computation");
	}

	/* Dump entropies and ptune results. */
	dump_ent_and_ptune(cookie.ent, cookie.ptune, epath, ppath, poss->rows);
	if (cookie.ent != NULL)
		free(cookie.ent);
	if (cookie.ptune != NULL)
		free(cookie.ptune);
}

static
void
make_cauchy_col_p(size_t id, struct make_dist_cookie *cookie)
{
	const struct matrix *dist, *poss;
	float gamma, *sum_dists;
	size_t j, i;

	/* Extract arguments */
	dist = cookie->dist;
	poss = cookie->poss;
	gamma = cookie->parms[0];
	sum_dists = cookie->scratch;
	
	/* Get column index */
	j = id;

	/* Find distances */
	qsne_pdist_part(dist->data, dist->ld, poss->data, poss->ld, poss->rows, poss->cols, j, 1u);

	/* Put Inf on the diagonal to ignore self */
	dist->data[j + j * dist->ld] = (float)HUGE_VAL;
	/* Put Inf on the padding */
	for (i = dist->rows; i < dist->ld; ++i)
		dist->data[i + j * dist->ld] = (float)HUGE_VAL;

	/* Get normal density */
	sum_dists[j] = qsne_cauchy(&dist->data[j * dist->ld], &dist->data[j * dist->ld], gamma, dist->ld);
}

static
void
scale_cauchy_col_p(size_t id, struct make_dist_cookie *cookie)
{
	const struct matrix *dist;
	float sum_dist;
	size_t j;

	/* Extract arguments */
	dist = cookie->dist;
	sum_dist = cookie->parms[0];
	/* Get column index */
	j = id;

	/* Normalize & symmetrize */
	qsne_scale(&dist->data[j * dist->ld], 1.f / sum_dist, dist->rows);
}

static
float
make_cauchy(float *scratch, const struct matrix *dist, const struct matrix *poss, float gamma)
{
	struct make_dist_cookie cookie;
	float sum_dist;
	size_t j;

	/* Create cookie */
	cookie.dist = dist;
	cookie.poss = poss;
	cookie.scratch = scratch;

	/* Compute densities */
	cookie.parms[0] = gamma;
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < poss->rows; ++id)
		make_cauchy_col_p(id, &cookie);
	}}

	/* Compute normalizer */
	sum_dist = 0.;
	for (j = 0; j < poss->rows; ++j)
		sum_dist += scratch[j];

	/* Scale */
	cookie.parms[0] = sum_dist;
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < poss->rows; ++id)
		scale_cauchy_col_p(id, &cookie);
	}}

	return sum_dist;
}

struct kld_cookie {
	const struct matrix *in_dist;
	const struct matrix *out_dist;
	float *scratch;
};

static
void
compute_kld_p(size_t id, struct kld_cookie *cookie)
{
	const struct matrix *in_dist, *out_dist;
	size_t j;
	float *klds;

	/* Extract arguments */
	in_dist = cookie->in_dist;
	out_dist = cookie->out_dist;
	klds = cookie->scratch;
	/* Get column index */
	j = id;

	/* Compute */
#ifdef USE_VLN
	klds[j] = qsne_kld_vln(&in_dist->data[j * in_dist->ld], in_dist->ld, &out_dist->data[j * out_dist->ld], out_dist->ld, in_dist->rows, 1u);
#else
	klds[j] = qsne_kld(&in_dist->data[j * in_dist->ld], in_dist->ld, &out_dist->data[j * out_dist->ld], out_dist->ld, in_dist->rows, 1u);
#endif
}

static
float
compute_kld(float *scratch, const struct matrix *in_dist, const struct matrix *out_dist)
{
	struct kld_cookie cookie;
	float sum_kld;
	size_t j;
	TIMER_START("tmp");

	/* Create cookie */
	cookie.in_dist = in_dist;
	cookie.out_dist = out_dist;
	cookie.scratch = scratch;

	/* Compute KL divergence */
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < in_dist->cols; ++id)
		compute_kld_p(id, &cookie);
	}}

	/* Collect */
	sum_kld = 0.;
	for (j = 0; j < in_dist->cols; ++j)
		sum_kld += scratch[j];

	TIMER_END("tmp");
	GET_TIMER("kld_computation")->acc += GET_TIMER("tmp")->acc;
	return sum_kld;
}

struct make_grad_cookie {
	const struct matrix *grad, *in_dist, *out_dist, *out_poss;
	float pscale;
	size_t work_size;
};

static
void
make_grad_p(size_t id, struct make_grad_cookie *cookie)
{
	const struct matrix *grad, *in_dist, *out_dist, *out_poss;
	size_t col, ccol;

	/* Extract arguments */
	grad = cookie->grad;
	in_dist = cookie->in_dist;
	out_dist = cookie->out_dist;
	out_poss = cookie->out_poss;

	/* Get indices */
	col = cookie->work_size * id;
	ccol = cookie->work_size;
	if (out_poss->rows - col < ccol)
		ccol = out_poss->rows - col;

	/* Compute */
	qsne_cauchygrad_part(grad->data, grad->ld, in_dist->data, in_dist->ld, out_dist->data,
		out_dist->ld, out_poss->data, out_poss->ld, out_poss->rows, out_poss->cols, col, ccol, cookie->pscale);
}

static
size_t
divide_work(size_t count, size_t size, size_t workers)
{
	size_t slab, line;

	/* Divide work equally, at appropriate multiples */
	slab = padcount((count + (workers - 1u)) / workers, size);

	/* Limit by line size */
	line = padcount(1u, size);
	if (slab < line)
		slab = line;

	/* Slab too large? */
	if (slab > count)
		return count;

	return slab;
}

static
int
get_num_threads(void)
{
#ifdef _OPENMP
	return omp_get_num_threads();
#else
	return 1;
#endif
}

static
void
make_grad(const struct matrix *grad, const struct matrix *in_dist, const struct matrix *out_dist, const struct matrix *out_poss, float pscale)
{
	struct make_grad_cookie cookie;
	size_t jobs;

	/* Divide work */
	cookie.work_size = divide_work(out_poss->rows, sizeof(*grad->data), (size_t)get_num_threads());
	jobs = (out_poss->rows + (cookie.work_size - 1u)) / cookie.work_size;

	/* Create cookie */
	cookie.grad = grad;
	cookie.in_dist = in_dist;
	cookie.out_dist = out_dist;
	cookie.out_poss = out_poss;
	cookie.pscale = pscale;

	/* Compute gradient */
	{{ size_t id;
#pragma omp parallel for schedule(static)
	for (id = 0; id < jobs; ++id)
		make_grad_p(id, &cookie);
	}}
}

static
int
armijo_check(const struct gdm_ctx *gdm_ctx, float alpha, float obj)
{
	float pred;

	/* Compute prediction */
	pred = gdm_ctx->ls_obj + gdm_ctx->ls_coef * alpha * gdm_ctx->ls_slope;

	if (!(obj <= pred))
		DPRINTF("backing out: alpha=%g last_obj=%g obj=%g (want=%g excess=%g)\n",
			alpha, gdm_ctx->ls_obj, obj, pred, obj - pred);

	/* Test */
	return obj <= pred;
}

static
float
do_update(float *scratch, struct gdm_ctx *gdm_ctx, struct matrix *out_poss, float *gamma, struct matrix *grad, struct matrix *out_dist, const struct matrix *in_dist, const int calc_kld)
{
	float sum_dist, obj;

	/* Compute output distribution */
	sum_dist = make_cauchy(scratch, out_dist, out_poss, *gamma);

	/* Compute objective *prior* to the update */
	 /* NOTE: This is slow--don't do it if not necessary */
	obj = -1.f;
	if (calc_kld)
		obj = compute_kld(scratch, in_dist, out_dist);

	/* Do a line search? */
	if (gdm_ctx->ls_coef >= 0.f && gdm_ctx->opt_mode == LBFGS)
	{
		float alpha;

		/* Get objective value if not computed already */
		if (!calc_kld)
			obj = compute_kld(scratch, in_dist, out_dist);

		/* Check for Armijo condition */
		alpha = 1.f;
		while (alpha > 0.f && !armijo_check(gdm_ctx, alpha, obj))
		{
			/* Update location */
			{{ size_t i;
			for (i = 0; i < out_poss->ld * out_poss->cols; ++i)
			{
				/* NOTE: Two steps here to avoid error accumulating */
				out_poss->data[i] -= alpha * gdm_ctx->deltas[i];
				out_poss->data[i] += (alpha * gdm_ctx->ls_step) * gdm_ctx->deltas[i];
			}
			}}

			/* Track alpha */
			alpha *= gdm_ctx->ls_step;

			/* Recompute distribution */
			sum_dist = make_cauchy(scratch, out_dist, out_poss, *gamma);

			/* Recompute objective */
			obj = compute_kld(scratch, in_dist, out_dist);
		}
	}

	/* Compute gradient */
	make_grad(grad, in_dist, out_dist, out_poss, gdm_ctx->pscale);

	/* Scale gradient */
	qsne_scale(grad->data, 4.f * sum_dist * (*gamma * *gamma), grad->ld * grad->cols);

	/* Zero excess elements-- BFGS computes dots */
	{{ size_t j, i;

		for (j = 0; j < out_poss->cols; ++j)
			for (i = out_poss->rows; i < out_poss->ld; ++i)
				out_poss->data[i + j * out_poss->ld] = 0.f;

		for (j = 0; j < out_poss->cols; ++j)
			for (i = out_poss->rows; i < out_poss->ld; ++i)
				grad->data[i + j * out_poss->ld] = 0.f;
	}}

	/* Get update direction */
	gdm_update(gdm_ctx, out_poss->data, grad->data);

	/* Store objective if update was done */
	if (obj != -1.f)
		gdm_ctx->ls_obj = obj;

	/* Compute projection for line search */
	if (gdm_ctx->ls_coef >= 0.f)
	{
		/* Compute slope */
		gdm_ctx->ls_slope = 0.f;
		{{ size_t i;
		for (i = 0; i < grad->ld * grad->cols; ++i)
			gdm_ctx->ls_slope += gdm_ctx->deltas[i] * grad->data[i];
		}}
	}

	/* Update */
	{{ size_t i;
	for (i = 0; i < out_poss->ld * out_poss->cols; ++i)
		out_poss->data[i] += gdm_ctx->deltas[i];
	}}

	/* Center data */
	if (gdm_ctx->compat_opts.o.CENTER && gdm_ctx->opt_mode == GDM) {
		if (gdm_ctx->mean.data == NULL)
			matrix_new(&gdm_ctx->mean, out_poss->cols, 1);
		matrix_center(out_poss, gdm_ctx->mean.data);
	}

	return obj;
}

static
float
do_final(float *scratch, const struct matrix *out_poss, const float *gamma, const struct matrix *out_dist, const struct matrix *in_dist)
{
	/* Compute output distribution */
	(void)make_cauchy(scratch, out_dist, out_poss, *gamma);

	/* Compute objective */ 
	return compute_kld(scratch, in_dist, out_dist);
}

static
void
dump_ent_and_ptune(const float *ent, const float *ptune, const char *epath, const char *ppath, size_t rows)
{
	   if (epath != NULL)
		   dump_tsv_to_file(epath, ent, rows, rows, 1);
	   if (ppath != NULL)
		   dump_tsv_to_file(ppath, ptune, rows, rows, 2);
}

/* Given a prefix and iteration number, get full Y path. */
char*
get_y_path(const size_t i, const char *dir, char *buf)
{
	#define MAX_DIGITS (32)
	size_t s, c;
	s = strlen(dir)+6+MAX_DIGITS;
	if (buf == NULL)
		buf = malloc(s+1);
	c = snprintf(buf, s, "%s_y_%ld.tsv", dir, i);
	buf[c] = '\0';
	return buf;
}

static
void
void_fclose(void *cookie)
{ (void)fclose((FILE *)cookie); }

int
main(int argc, char **argv)
{
	struct opts opts;
	const char *in_fn;
	struct cupstack cupstack;
	float *scratch;
	struct matrix in_poss, out_poss, in_dist, out_dist, grad;
	struct gdm_ctx upd_ctx;
	float gamma;
	FILE *kld_fp, *p_fp;
	char *y_path_buf;
	const char *err_file;
	TIMER_START("all");
	y_path_buf = NULL;
	p_fp = NULL;

	/* Set up default settings */
	default_opts(&opts);

	/* Parse arguments */
	{{
		int ind;

		/* Parse */
		ind = parse_opts(&opts, argc, argv);
		if (ind != argc - 1)
		{
			/* Print usage */
			usage(argv[0]);
			return EXIT_FAILURE;
		}

		/* Get input file */
		in_fn = argv[ind];

		/* Set up cleanup stack */
		cupstack_init(&cupstack);
	}}

	/* Checks */
	{{
		if ((opts.ptune_fn != NULL) && (opts.perp_hi <= opts.perp_lo)) {
			fprintf(stderr, "Can't dump ptune results with invalid perplexity range\n");
			goto err_gen;
		}

		/* Verify that outputs are writable */
		if ((err_file = verify_outs(&opts)) != NULL)
			goto err_write;

#ifdef VERBOSE
	enabled_flags(opts.compat_opts);
#endif
	}}

	/* Read input */
	{{
		const char *errmsg;

		/* Read */
		errmsg = read_tsv(&in_poss, in_fn);
		if (errmsg != NULL)
		{
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, errmsg);
			goto err_gen;
		}
		/* Add input for cleanup */
		cupstack_push(&cupstack, &free, in_poss.data);
	}}

	/* Start a worker pool */
	{{
		size_t scratch_size;

		/* Create pool */
#ifdef _OPENMP
		if (opts.num_workers != -1)
			omp_set_num_threads(opts.num_workers);
#endif

		/* Compute size for scratch */
		scratch_size = in_poss.rows;
		if (in_poss.cols > scratch_size)
			scratch_size = in_poss.cols;

		/* Allocate scratch for accumulating the parallel results */
		scratch = (float *)malloc(scratch_size * sizeof(*scratch));
		if (scratch == NULL)
			goto err_nomem;

		/* Add for cleanup */
		cupstack_push(&cupstack, &free, scratch);
		cupstack_swap(&cupstack);
	}}

	/* Generate initial condition */
	{{
		const char *errmsg;

		/* User specified? */
		if (opts.init_fn != NULL)
		{
			/* Read file */
			errmsg = read_tsv(&out_poss, opts.init_fn);
			if (errmsg != NULL)
			{
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, errmsg);
				goto err_gen;
			}

			/* Add for cleanup */
			cupstack_push(&cupstack, &free, out_poss.data);
			cupstack_swap(&cupstack);

			/* Check dimension */
			if (!(out_poss.rows == in_poss.rows && out_poss.cols < in_poss.cols))
			{
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.init_fn,
					"bad dimensions for initial condition");
				goto err_gen;
			}
		}
		else
		{
			/* Stop if we don't have the data and no IC.. */
			if (opts.in_format != IN_FORMAT_RAW)
			{
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, "missing initial condition");
				goto err_gen;
			}

			/* Generate initial condition using PCA */
			errmsg = init_pca(&out_poss, opts.outcols, &in_poss);
			if (errmsg != NULL)
			{
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, errmsg);
				goto err_gen;
			}

			/* Add for cleanup */
			cupstack_push(&cupstack, &free, out_poss.data);
			cupstack_swap(&cupstack);
		}
	}}

	/* Compute input distribution */
	{{
		TIMER_START("indist_computation");

		/* If distance matrix given, check dimensions */
		if (!(opts.in_format == IN_FORMAT_RAW))
			if (!(in_poss.rows == in_poss.cols))
			{
				fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, "a distance matrix must be square");
				goto err_gen;
			}

		/* Allocate */
		if (matrix_new(&in_dist, in_poss.rows, in_poss.rows) == NULL)
			goto err_nomem;

		/* Load if precomputed */
		if (opts.p_fn != NULL && (p_fp = fopen(opts.p_fn, "rb")) != NULL) {
			DPRINTF("Loading P matrix %s\n", opts.p_fn);
			if (load_smat_bin(p_fp, &in_dist) == NULL)
				goto err_gen;
			fclose(p_fp);
		}
		else {
			/* Compute */
			make_normal(scratch, &in_dist, &in_poss, opts.perp_lo, opts.perp_hi, opts.ent_fn, opts.ptune_fn, opts.in_format);
		}

		/* We can give up the input positions now */
		cupstack_pop(&cupstack);
		/* Add in_dist to the stack */
		cupstack_push(&cupstack, &free, in_dist.data);
		TIMER_FINISH("indist_computation");
	}}

	/* Allocate memory & open file handles */
	{{
		/* Allocate output distribution */
		if (matrix_new(&out_dist, out_poss.rows, out_poss.rows) == NULL)
			goto err_nomem;

		/* Push */
		cupstack_push(&cupstack, &free, out_dist.data);

		/* Allocate space for the gradient */
		if (matrix_new(&grad, out_poss.rows, out_poss.cols) == NULL)
			goto err_nomem;

		/* Push */
		cupstack_push(&cupstack, &free, grad.data);

		/* Allocate optimizer-specific data */
		if (gdm_init(&upd_ctx, out_poss.ld * out_poss.cols, &opts)) {
			fprintf(stderr, "%s: %s: %s" "\n", argv[0], opts.init_fn,
					"Failed to setup optimizer");
			goto err_gen;
		}

		/* Push */
		cupstack_push(&cupstack, (void (*)(void *))&gdm_deinit, &upd_ctx);

		/* Objective */
		kld_fp = NULL;
		if (opts.kld_fn != NULL) {
			if ((kld_fp = fopen(opts.kld_fn, "w")) == NULL) {
				err_file = opts.kld_fn;
				goto err_write;
			}
			cupstack_push(&cupstack, &void_fclose, kld_fp);
		}

		/* Y-values */
		if (opts.y_dir_fn != NULL) {
			y_path_buf = get_y_path(upd_ctx.iteration, opts.y_dir_fn, y_path_buf);
			if (y_path_buf  == NULL)
				goto err_nomem;
			cupstack_push(&cupstack, &free, y_path_buf);
		}

		/* Save P-matrix if file doesn't exist */
		if (opts.p_fn != NULL && p_fp == NULL) {
			if ((p_fp = fopen(opts.p_fn, "wb")) == NULL) {
				err_file = opts.p_fn;
				goto err_write;
			}
			cupstack_push(&cupstack, &void_fclose, p_fp);

			DPRINTF("Saving P-matrix to %s\n", opts.p_fn);
			dump_smat_bin(&in_dist, p_fp);
		}

		/* Start with gamma = 1 */
		gamma = 1.f;
	}}

	/* Optimize */
	{{
		float obj, obj_l, tol;
		int dump_kld;
		tol = 0.f;

		TIMER_START("optimization");
		DPRINTF("Starting optimization");

		/* Loop */
		for (upd_ctx.iteration = 0; upd_ctx.iteration < opts.max_iters; ++upd_ctx.iteration)
		{
			/* Set proper tolerance after some iterations */
			if (upd_ctx.iteration == MIN_ITERS)
				tol = opts.min_delta;

			dump_kld = !(upd_ctx.iteration % opts.kld_dump_interval);
			/* Remove exaggeration */
			if (upd_ctx.iteration == upd_ctx.stop_lying_iter && upd_ctx.compat_opts.o.SCALE)
				upd_ctx.pscale = 0.f;

			/* Get last obj (-1.f if no update was done) */
			obj_l = upd_ctx.ls_obj;

			/* Make an update */
			obj = do_update(scratch, &upd_ctx, &out_poss, &gamma, &grad, &out_dist, &in_dist, dump_kld);

			/* Objective was updated? */
			if (obj != -1.f) {

				/* Numerical instability? */
				if (!isfinite(obj)) {
					fprintf(stderr, "Objective not finite, aborting. iter=%lu\n", upd_ctx.iteration);
					break;
				}

				/* Print progress? */
				if (dump_kld) {
					DPRINTF("iter=%lu, obj=%f, rel_diff=%e\n", upd_ctx.iteration, obj, (double) fabsf(obj - obj_l)/obj_l);
					SFPRINTF(kld_fp, "%lu\t%.15g\n", upd_ctx.iteration, (double)obj);

					/* Dump Y-values */
					if (opts.y_dir_fn != NULL) {
						if ((y_path_buf = get_y_path(upd_ctx.iteration, opts.y_dir_fn, y_path_buf)) == NULL)
							goto err_nomem;
						dump_tsv_to_file(y_path_buf, out_poss.data, out_poss.ld, out_poss.rows, out_poss.cols);
					}
				}

				/* Early exit? */
				if (obj_l != -1.f && !isinf(tol) &&
						fabsf(obj - obj_l) < tol * obj_l) {
					DPRINTF("Early exit iter=%lu, obj=%f, rel_diff=%e\n", upd_ctx.iteration, obj, (double) fabsf(obj - obj_l)/obj_l);
					break;
				}
			}
		}

		/* Compute final objective */
		obj = do_final(scratch, &out_poss, &gamma, &out_dist, &in_dist);
		SFPRINTF(kld_fp, "%lu\t%.15g\n", upd_ctx.iteration, (double)obj);
		TIMER_FINISH("optimization");
	}}

	/* Scale data */
	{{
		size_t j;
		for (j = 0; j < out_poss.cols; ++j)
			qsne_scale(&out_poss.data[j*out_poss.ld], 1.f / gamma, out_poss.rows);
	}}
	TIMER_FINISH("all");

	/* Write results */
	{{
		/* Output file given? */
		if (opts.out_fn != NULL)
		{
			err_file = opts.out_fn;
			if (dump_tsv_to_file(err_file, out_poss.data, out_poss.ld, out_poss.rows, out_poss.cols))
				goto err_write;
		}
		else
			/* Dump to stdout */
			dump_tsv(stdout, out_poss.data, out_poss.ld, out_poss.rows, out_poss.cols);
	}}

	/* Free data */
	cupstack_deinit(&cupstack);
	return EXIT_SUCCESS;

err_nomem:
	cupstack_deinit(&cupstack);
	fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn, "no memory");
	return EXIT_FAILURE;

err_write:
	fprintf(stderr, "%s: %s: %s" "\n", argv[0], in_fn,
						"failed to open for write");
err_gen:
	cupstack_deinit(&cupstack);
	return EXIT_FAILURE;
}
