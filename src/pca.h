#ifndef PCA_H_
#define PCA_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Compute PCA coefficients from data */
void pca(float *coefs, float *covs, const float *data, size_t ldda,
	size_t samps, size_t dims);

/* Compute PCA coefficients from a covariance matrix */
void pcacov(float *coefs, float *covs, size_t dims); 

#ifdef __cplusplus
}
#endif

#endif /* PCA_H_ */
