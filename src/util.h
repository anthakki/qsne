#ifndef UTIL_H_
#define UTIL_H_

#include <stdlib.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Align to 64B cache lines */
#define QSNE_ALIGN ((size_t)64u)

/* Allocate a zeroed array with proper alignment and padding. */
void *acalloc(size_t count, size_t size);
/* Compute the padding (in elements) required by an array. */
size_t padcount(size_t count, size_t size);
/* Write float matrix as tab-separated file to dest */
void dump_tsv(FILE *dest, const float *data, size_t ld, size_t rows, size_t cols);
/* Same as dump_tsv, but automatically open the file. Returns 0 on success, -1 on error. */
int dump_tsv_to_file(const char *fpath, const float *data, size_t ld, size_t rows, size_t cols);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_H_ */
