#include "util.h"
#include <string.h>

size_t
padcount(size_t count, size_t size)
{
	size_t mult;

	/* Find number of items per alignment */
	mult = QSNE_ALIGN / size;
	if (mult < 1)
		mult = 1;

	/* Round up to next multiple */
	return (( (count + (mult - 1)) / mult )) * mult;
}

void *
acalloc(size_t count, size_t size)
{
	size_t bytes;
	void *data;

	/* Pad size */
	bytes = padcount(count, size) * size;

	/* Allocate */
	if (posix_memalign(&data, QSNE_ALIGN, bytes) != 0)
		return NULL;
	/* Zero memory */
	memset(data, 0, bytes);

	return data;
}

void
dump_tsv(FILE *dest, const float *data, size_t ld, size_t rows, size_t cols)
{
	size_t i, j;
	const char *sep;

	/* Loop through the columns */
	for (i = 0; i < rows; ++i)
	{
		/* Write a row of data */
		sep = "";
		for (j = 0; j < cols; ++j)
		{
			(void)fprintf(dest, "%s%.15g", sep, (double)data[i + j * ld]);
			sep = "\t";
		}

		/* Put a newline */
		(void)fprintf(dest, "\n");
	}
}

int
dump_tsv_to_file(const char *fpath, const float *data, size_t ld, size_t rows, size_t cols)
{
	FILE *fp;
	if (fpath == NULL || data == NULL)
		return -1;
	fp = fopen(fpath, "w");
	if (fp == NULL)
		return -1;
	dump_tsv(fp, data, ld, rows, cols);
	fclose(fp);
	return 0;
}
