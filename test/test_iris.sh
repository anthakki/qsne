#!/bin/sh

# get data
./test/get_iris.R >test/iris.in

# standard qSNE
./qsne -d2 -p30 -o test/iris.out.default test/iris.in
# Rtnse compatibility
./qsne -d2 -p30 -C -o test/iris.out.compat test/iris.in

# check separability
./test/check-sep.R test/iris.out.default
./test/check-sep.R test/iris.out.compat

# do scatterplot for comparison
./test/plot-scatter-pair.R test/iris.out.default test/iris.out.compat

# done
echo 'success'
