#!/bin/sh

## Get the Fisher iris data from MATLAB

# use MATLAB to dump the dataset
matlab -nodesktop -nojvm -r " \
	fprintf(2, '%g\t%g\t%g\t%g\n', iris_dataset); \
	quit; \
" 2>&1 >/dev/null
