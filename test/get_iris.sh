#!/bin/sh

## Download the Fisher iris data over internet

# see http://archive.ics.uci.edu/ml/datasets/Iris
URL='http://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'

# get data
curl -L "$URL" | sed "$(printf '/^$/d; s@,@\\t@g; s@\.0\\t@\\t@g; s@\.0$@@g')" | cut -f1-4
