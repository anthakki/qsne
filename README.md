
qSNE -- Quadratic rate t-SNE optimizer with automatic parameter tuning
======================================================================

qSNE is a [t-SNE](https://en.wikipedia.org/wiki/t-SNE) implementation with potentially quadratic converge rate (vs. linear) and automatic perplexity tuning. The implementation solves the exact [original t-SNE problem][2] and not an approximate version.

Major features:

- A quasi-Newton optimizer which allows quadratic vs. linear rate of converge and, consequently, gives a similar result in e.g. &#8730;1000 ~ 30 iterations versus 1000

- A perplexity range instead of a fixed value can be specific, in which case the perplexity is automatically optimized

- Fully supports Intel [SSE](https://en.wikipedia.org/wiki/Streaming_SIMD_Extensions) and [AVX](https://en.wikipedia.org/wiki/Advanced_Vector_Extensions) (optional) and [POSIX threads](https://en.wikipedia.org/wiki/POSIX_Threads) for parallelization 

For the details about the method, please refer to our [publication][1] on the matter.

[1]: https://
[2]: http://www.jmlr.org/papers/volume9/vandermaaten08a/vandermaaten08a.pdf

Installation
------------

qSNE can be compiled into a set of standalone programs (in which case MATLAB or R is not needed) or used through MATLAB interfaces. There is also an R wrapper invoking the command line program. 

To build qSNE, you will need an ISO C90 (or ANSI C89) compiler (such as [GCC](https://gcc.gnu.org/) or [Clang](https://clang.llvm.org/)), [GNU make](https://www.gnu.org/software/make/), ISO C99 math library (included in most operating systems), POSIX threads (included in most UNIX-like systems such as Linux and macOS), and [LAPACK](http://www.netlib.org/lapack/). If you want to build the documentation (optional), you will need [Pandoc](https://pandoc.org/).
 
qSNE has been tested on Ubuntu 18.04 with gcc 7.4.0, gmake 4.1, and LAPACK 3.7.1.

To build the binaries, run `make` and copy binaries to your preferred location (this should take a few seconds):

```
	make 
	sudo install qsne /usr/local/bin/
```

This will build a static library `libqsne.a`, a shared library `libqsne.so`, and a command line interface program [**qsne**](qsne.1.md) which is linked statically to `libqsne`. Run `make man` to build the documentation. If you need to customize the build parameters, such as disable SSE or AVX support, please edit [`Makefile`](Makefile) to suit your needs.

NOTE: when building with `-march=native` (default), binaries built on a particular system might not run on older hardware as hardware-specific optimizations are enabled. This is a common source of problems when moving the analysis from desktop to cloud. Either rebuild the binary on the target machine or specify `-march` accordingly.

qSNE is also available as an [Anduril component](https://www.anduril.org/anduril/bundles/all/doc/components/QSNE/index.html), which facilitates integration with other tools.

General usage
-------------

qSNE can be used through the standalone program [**qsne**](qsne.1.md) built using the above instructions. Please refer to the man page for a complete reference of the details.

General usage example follows:

```
	qsne -d 2 -p 20:40 -m 10 -o output.tsv input.tsv
```

This reads the data from the input file `input.tsv`, optimizes automatically the perplexity within the range of 20 to 40 (**-p 20:40**), selects a rank-10 Hessian approximation (**-m 10**), and maps the data into 2D (**-d 2**) using the default optimizer parameters (see [reference](qsne.1.md)). The result is output to `output.tsv`.

The input and output files are assumed to be tab- or space-separated ASCII text of decimal numbers the rows correspond to samples and the columns variables (such as genes or the different markers of an experiment).

Short comment about the parameter selection: The dimension (**-d**) is dictated by the application, typically 2D or 3D are used for visualization, but any number can be used. The perplexity (**-p**) represents a rough number of neighboring samples to whom distances are to be retained. The choice of this value depends both on the sample size and on the desired level of detail. The number of iterations (**-k**) is usually not very important and can be fixed to a large value, as qSNE usually converges in very few iterations and will exit after detecting this (see **-t**). The Hessian rank (**-m**) should be set to whatever is the upper bound for the inherent local linear dimension of the data. This would be very expensive to estimate, but, fortunately, even small values tend to be very useful in practice, and there is no disadvantage (besides computational effort) of selecting too high a number. The [original][2] gradient descent algorithm is recovered at **-m 0**.

SIMD vectorization is always used, as this imposes virtually no penalty. To control the number of worker threads (to parallelize on multiple cores), specify the flag **-T** (default is to use all CPU cores).

There is a demo that uses the [Fisher iris dataset](http://archive.ics.uci.edu/ml/datasets/Iris) in [test/test_iris.sh](test/test_iris.sh) (requires R). This will plot the image shown below and print `success` in case qSNE is functioning correctly (this will take few seconds):

![Example mappings for the Fisher iris dataset](test/test_iris_example.png)

Using qSNE through MATLAB
-------------------------

To build the [MATLAB](https://www.mathworks.com/products/matlab.html) interfaces, issue:

```
	make -C matlab
```

This will build `libqsne` if necessary and the MATLAB wrappers accessing the library. In MATLAB, use [`addpath()`](https://www.mathworks.com/help/matlab/ref/addpath.html) to add the path of the interfaces to your MATLAB search path. The function [`qsne()`](matlab/qsne.m) serves as a high-level routine for t-SNE computation. Type `help qsne` on the MATLAB prompt to print the documentation for this routine. All parameters are customizable through the MATLAB interface.

Typical usage can be seen in the example below. Optimization can be done using a fixed or adjusted perplexity value. If the optimization has to be repeated many times, but with identical perplexity setting, one can use precomputed P matrix and pass it to the optimization procedure.
```
	% Get input data
	X=iris_dataset';
	% Generate random solution
	Y0=randn(size(X,1), 2);

	perp=5;
	% Optimizer parameters
	hess_rank=10; max_iters=1000; tol=1e-6;
	% Optimize with fixed perplexity
	Y1=qsne(X, Y0, perp, hess_rank, max_iters, tol);
	% Optimize and choose perplexity values automatically
	Y2=qsne(X, Y0, [3 15], hess_rank, max_iters, tol);

	% Precompute input similarities
	P=make_normal(X, perp);
	Y3=qsne(X, Y0, [], hess_rank, max_iters, tol, 'P', P);

	% Use precomputed P matrix with perplexity tuning
	[P, tuned_perp]=make_normal(X, [3 15]);
	Y4=qsne(X, Y0, [], hess_rank, max_iters, tol, 'P', P);
```

qSNE has been tested with MATLAB R2019a.

Using qSNE through R
--------------------

For [R](https://www.r-project.org/) users, there is a wrapper script for the `qsne` command line tool. This requires that you have installed `qsne` as above into a directory that is the system $PATH. As this function communicates between R and `qsne` using temporary files, it may result in poor performance if used frequently in an inner loop of your code, but it is well suited for running large individual analyses. 

To use the script, load it into your R environment:

```
	source('r/qsne.R')
```

This will define the function [`qsne()`](r/qsne.R) which uses an interface similar to [Rtsne](https://cran.r-project.org/web/packages/Rtsne/) but also features the qSNE-specific parameters.

qSNE has been tested with R 3.6.2.

Copying
-------

All files are subject to the simplified BSD license. Please refer to [`LICENSE.txt`](LICENSE.txt) for details. Copyright (c) 2018-2019 Antti Hakkinen and Juha Koiranen.
