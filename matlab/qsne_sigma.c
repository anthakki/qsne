
#include <mex.h>
#include <qsne.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (qsne_padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (qsne_padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_sigma"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *dists;
	mwSize rows, cols, j;
	float ent, ent_max;
	float *sigmas;

	/* Check input counts */
	if (nrhs < 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Check data */
	if (!(isFullReal(prhs[0]) && mxIsSingle(prhs[0]) && isMatrix(prhs[0]) && checkSinglePtr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":dists", "The input dists must be a padded full real single matrix.");
	if (!(isFullReal(prhs[1]) && mxIsSingle(prhs[1]) && isScalar(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":ent", "The input ent must be a single scalar.");

	/* Get data */
	dists = (const float *)mxGetData(prhs[0]);
	rows = mxGetM(prhs[0]);
	cols = mxGetN(prhs[0]);
	ent = *(const float *)mxGetData(prhs[1]);

	/* Check optional parameters */
	ent_max = ent;
	if (nrhs >= 3 && !mxIsEmpty(prhs[2]))
	{
		if (!(isFullReal(prhs[2]) && mxIsSingle(prhs[2]) && isScalar(prhs[2])))
			mexErrMsgIdAndTxt(MEXFILENAME ":ent", "The input ent_max must be a single scalar.");
		ent_max = *(const float *)mxGetData(prhs[2]);
	}

	/* Create output array */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(1, cols, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	sigmas = (float *)mxGetData(plhs[0]);
	for (j = 0; j < cols; ++j)
		sigmas[j] = qsne_sigma_tune(&dists[j * rows], ent, ent_max, rows);
}
