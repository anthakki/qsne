
function G= cauchygrad(P, Q, Y, Z)
    %%
    if nargin < 4 || isempty(Z)
        Z= 1;
    end
    r= size(Y, 1);

    zpad= @(x) qsne_copy(qsne_copy(x, 0).', 0).';
    if ~isqmat(P), P=zpad(P); end
    if ~isqmat(Q), Q=zpad(Q); end
    if ~isqmat(Y), Y=qsne_copy(Y,0); end

    G= qsne_cauchygrad( P, Q, Y );
    G= Z * G(1:r, :);
