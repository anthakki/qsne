#include <mex.h>
#include <qsne.h>
#include <lbfgs/lbfgs_ctx.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isColumn(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (qsne_padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (qsne_padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_lbfgs_iter"

#define FULLREAL (1u)
#define SINGLE   (1u << 1)
#define ALIGN    (1u << 2)
#define SCALAR   (1u << 3)

char *
doCheck(const mxArray *a, unsigned c)
{
	if ((FULLREAL & c) && !isFullReal(a))
		return "must be full real";
	if ((SINGLE & c) && !mxIsSingle(a))
		return "must be single";
	if ((ALIGN & c) && !checkSinglePtr(a))
		return "must be padded";
	if ((SCALAR & c) && !isFullReal(a))
		return "must be scalar";
	return NULL;
}

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *dists;
	mwSize rows, dims;

	const float *grads, *points;
	mwSize rb_head, rb_size;
	float damp;
	int argc = 5;

	/* Check input counts */
	if (nrhs < argc)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > argc)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

#define CHECK(p,c,m) do { const char *CC; if((CC = doCheck(p, c)) != NULL) \
	mexErrMsgIdAndTxt(MEXFILENAME m, m " %s", CC); } while(0)

	CHECK(prhs[0], FULLREAL | SINGLE | ALIGN, ":grads");
	CHECK(prhs[1], FULLREAL | SINGLE | ALIGN, ":points");
	CHECK(prhs[2], FULLREAL | SINGLE | SCALAR, ":rbhead");
	CHECK(prhs[3], FULLREAL | SINGLE | SCALAR, ":rbsize");
	CHECK(prhs[4], FULLREAL | SINGLE | SCALAR, ":damp");

	/* Get data */
	grads = (const float *)mxGetData(prhs[0]);
	points = (const float *)mxGetData(prhs[1]);
	rows = mxGetM(prhs[0]);
	damp = *((float*) mxGetData(prhs[4]));
	rb_head = (int) *((float*) mxGetData(prhs[2]));
	rb_size = (int) *((float*) mxGetData(prhs[3]));

	if (rb_head > rb_size)
		mexErrMsgIdAndTxt(MEXFILENAME ":rbhead", "Ring buffer head greater than size");
	if (!(rb_size > 0))
		mexErrMsgIdAndTxt(MEXFILENAME ":rbsize", "Ring buffer size must be > 0");

	/* Create output array */
	(void)nlhs;
	/* Gradient */
	plhs[0] = mxCreateNumericMatrix(rows, 1, mxSINGLE_CLASS, mxREAL);
	/* Search direction */
	plhs[1] = mxCreateNumericMatrix(rows, 4, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	mxArray *a = mxCreateNumericMatrix(rb_size, 1, mxSINGLE_CLASS, mxREAL);

	calc_search_dir((float*)grads, (float*)points, (float*)mxGetData(plhs[1]), (float*)mxGetData(a), rows, (rb_head-1), rb_size, damp, rows);
}
