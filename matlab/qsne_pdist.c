
#include <mex.h>
#include <qsne.h>
#include <util.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_pdist"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *data;
	mwSize rows, cols;

	/* Check input counts */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Check data */
	if (!(isFullReal(prhs[0]) && mxIsSingle(prhs[0]) && isMatrix(prhs[0]) && checkSinglePtr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":data", "The input data must be a padded full real single matrix.");

	/* Get data */
	data = (const float *)mxGetData(prhs[0]);
	rows = mxGetM(prhs[0]);
	cols = mxGetN(prhs[0]);

	/* Create output array */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(rows, rows, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	qsne_pdist((float *)mxGetData(plhs[0]), rows, data, rows, rows, cols);
}
