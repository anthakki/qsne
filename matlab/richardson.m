
% RICHARDSON Estimate gradient and Hessian using Richardson extrapolation.
%   [gradient,Hessian]= RICHARDSON(f,x) estimates the gradient and the
%   Hessian using Richardson extrapolation.

% Last update: May 11, 2015
% Copyright (c) 2015 Antti Hakkinen

function [gradient, hessian]= richardson(f, x, order, delta)
	%% apply default arguments
	if nargin < 3 || isempty(order)
		order= 6;
	end
	if nargin < 4 || isempty(delta)
		delta= 1e-4; % NB. the settings were stolen from R numDeriv package
	end
	%% settings
	delta0= delta;
	ratio= 2;
	tol0= sqrt(eps / 7e-7);
	%% evaluate function
	fx= f(x);
	assert(isscalar(fx), ...
		'The input F must be a scalar function.');
	%% create arrays
	gradient= zeros(numel(x), 1, 'like', x);
	hessian= zeros(numel(x), 'like', x);
	[gdifs, hdifs]= deal(zeros(1, order, 'like', x));
	%% precompute stuff
	inds= reshape(1:numel(x), size(x));
	d0= delta * x + delta0 * (abs(x) < tol0);
	%% compute gradient & diagonal of the Hessian
	for i= 1:numel(x)
		%% compute finite differences
		di= d0(i);
		for k= 1:order
			%% evaluate function
			fa= f(x + di*(inds == i));
			fb= f(x - di*(inds == i));
			%% get differences
			gdifs(k)= (fa-fb) / (di+di); % NB. error is O(di^2)
			if nargout >= 2
				hdifs(k)= (fa-fx + fb-fx) / (di*di); % NB. error is O(di*dj)
			end
			%% decrement step size
			di= di/ratio;
		end
		%% extrapolate
		gradient(i)= extrapolate_(gdifs, ratio*ratio);
		if nargout >= 2
			hessian(i,i)= extrapolate_(hdifs, ratio*ratio);
		end
	end
	%% compute Hessian off-diagonal
	if nargout >= 2
		for i= 1:numel(x)
			for j= i+1:numel(x)
				%% compute finite differences
				di= d0(i); dj= d0(j);
				for k= 1:order
					%% evaluate function
					fa= f(x + di*(inds == i) + dj*(inds == j));
					fb= f(x - di*(inds == i) - dj*(inds == j));
					%% get differences
					hdifs(k)= (fa-fx + fb-fx - ... % NB. error is O(di*dj)
						(hessian(i,i)*di*di + hessian(j,j)*dj*dj)) / (2*di*dj);
					%% decrement step
					di= di/ratio; dj= dj/ratio;
				end
				%% extrapolate
				hessian(i,j)= extrapolate_(hdifs, ratio*ratio);
				hessian(j,i)= hessian(i,j);				
			end
		end
	end
	
function result= extrapolate_(difs, ratio)
	%% iterate
	coeff= ratio;
	for i= 1:numel(difs)-1
		%% update
		difs(1:end-i)= (coeff*difs((1:end-i)+1) - difs(1:end-i)) / (coeff-1);
		coeff= coeff*ratio;
	end
	%% return estimate
	result= difs(1);	
