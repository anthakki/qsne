
#include <mex.h>
#include <qsne.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (qsne_padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (qsne_padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_gamma"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *P, *Q, *Y;
	mwSize rows, cols;

	/* Check input counts */
	if (nrhs < 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 3)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Check data */
	if (!(isFullReal(prhs[0]) && mxIsSingle(prhs[0]) && isMatrix(prhs[0]) && checkSinglePtr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":P", "The input P must be a padded full real single matrix.");
	if (!(isFullReal(prhs[1]) && mxIsSingle(prhs[1]) && isMatrix(prhs[1]) && checkSinglePtr(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":Q", "The input Q must be a padded full real single matrix.");
	if (!(isFullReal(prhs[2]) && mxIsSingle(prhs[2]) && isMatrix(prhs[2]) && checkSinglePtr(prhs[2])))
		mexErrMsgIdAndTxt(MEXFILENAME ":Y", "The input Y must be a padded full real single matrix.");

	/* Get dtaa */
	P = (const float *)mxGetData(prhs[0]);
	Q = (const float *)mxGetData(prhs[1]);
	Y = (const float *)mxGetData(prhs[2]);
	rows = mxGetM(prhs[2]);
	cols = mxGetN(prhs[2]);

	/* Check dimensions */
	if (!(mxGetM(prhs[0]) == rows && mxGetN(prhs[0]) >= rows &&
			mxGetM(prhs[1]) == rows && mxGetN(prhs[1]) >= rows))
		mexErrMsgIdAndTxt(MEXFILENAME ":PQY", "invalid dimensoins");

	/* Create output array */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(rows, cols, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	qsne_cauchygrad((float *)mxGetData(plhs[0]), rows, P, rows, Q, rows, Y, rows, rows, cols);
}
