% MAKE_NORMALP Same as make_normal, but work with padded matrices
% m = number of points
function [out_p, tuned_perp]= make_normalp(in_x, in_perp, m)
    %%
    d= qsne_pdist(in_x);

    % put inf to diagonal and padding
    d(1:size(d,1)+1:end)= inf;
    d(m+1:end, :)= inf;
    d(:, m+1:end)= inf;

    %%
    ents= num2cell(log(single(in_perp)));
    sigmas= qsne_sigma(d, ents{:});

    p= nan(size(d), 'like', d);
    s= 0;
    for j= 1:m
        [p(:, j), sj]= qsne_normal(d(:, j), sigmas(j));
        % TODO: this is present in Rtsne
        p(:, j)= p(:, j) / sj;
        sj= single(1);
        assert(~isnan(sj));
        s= s + sj;
    end

    tuned_perp= exp(-sum(xlogy(p(:, 1:m)), 1).');

    p(:)= qsne_scale(p, 1 / s);
    p(:)= qsne_symmetrize(p);

    out_p= nan(size(in_x,1), 'like', in_x);
    out_p(:)= p;