% CAUCHYGRADP same as cauchygrad, but operate on padded matrices
% m= number of points
function G= cauchygradp(P, Q, Y, m, Z)
    %%
    if nargin < 5 || isempty(Z)
        Z= 1;
    end
    
    % put zeros to padding
    ld= ceil( size(P, 1) / 16 ) * 16;
    ii=((m+1):ld);
    P(ii, :)=0;
    Q(ii, :)=0;
    P(:, ii)=0;
    Q(:, ii)=0;
    Y(ii, :)=0;

    G= Z*qsne_cauchygrad( P, Q, Y );