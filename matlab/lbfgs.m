
function x= lbfgs(X, G)
	%%
	s= X(:, 2:end) - X(:, 1:end-1);
	y= G(:, 2:end) - G(:, 1:end-1);
		
% 	assert(all( diag( s'*y ) > 1.22e-4 ), ...
% 		'Curvature conditions violated-- not stable.');
	
	Q= nan(size(G));
	Q(:, end)= G(:, end);
	
	t= size(s, 2);
	
% 	% modify y for stability
% 	r= nan(1, t-1);
% 	for j= 1:t-1
% 		tau= 1 + max( -y(:,j)'*s(:,j)/(( s(:,j)'*s(:,j) )), 0 );
% % 		assert(tau == 1);
% % fprintf(1, 'tau = %g\n', tau);
% 		r(j)= 1.22e-4 * norm( G(:,j) ) + (tau-1);
% 		y(:,j)= y(:,j) + r(j) * s(:,j);
% 	end
	
	rho= nan(1, t);
	alpha= nan(1, t);
	
	for j= (t:-1:1)
		rho(j)= 1 / (( y(:,j)' * s(:,j) ));
		alpha(j)= rho(j) * (( s(:,j)'*Q(:,j+1) ));
		Q(:,j)= Q(:,j+1) - alpha(j)*y(:,j);
	end
	
% 	H0= eye(size(X, 1));
	gamma= 1;
	if t > 0
		gamma= (( y(:,end)'*s(:,end) / (( y(:,end)'*y(:,end) )) ));
	end
	H0= gamma * eye(size(X, 1));
	
	R= nan(size(G));
	R(:,1)= H0 * Q(:,1);
	
	beta= nan(1, t);
	for j= 1:t
		beta(j)= rho(j) * (( y(:,j)'*R(:,j) ));
		R(:,j+1)= R(:,j) + (( alpha(j)-beta(j) )) * s(:,j);
	end
	
	%%
	x= X(:,end) - R(:,end);
