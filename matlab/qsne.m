%QSNE Perform exact t-SNE using GDM or LBFGS optimizer
%   Y= qsne(X, Y, p, h, iters, tol, ...) runs t-SNE for input data X,
%   starting with solution Y using perplexity p. The perplexity can be
%   specified as a single value, or as a range so that an optimized value
%   is chosen automatically. Rank for hessian approximation is specified in
%   h and h=1 enables the use of original GDM optimization scheme.
%   Optimization is stopped after iters number of iterations, or when
%   relative difference in objective function falls below tol.
%
%   qsne(__, 'kldival', kldival) how often to dump KLD when using GDM
%   qsne(__,'P', P) precomputed P-matrix
%   qsne(__,'eta', eta) learning rate (default: 50)
%   qsne(__,'a1', a1) initial momentum (default: 0.5)
%   qsne(__,'a2', a2) latter momentum (default: 0.8)
%   qsne(__,'iter_mswitch', iter_mswitch) when to switch momentum and stop
%   exaggeration (default: 250)
%   qsne(__,'pscale', pscale) exaggeration factor (default: 4)
%   qsne(__,'damp', damp) damping factor for LBFGS (default: 0.8)
%   qsne(__,'ls_coef', ls_coef) line search coefficient (default: 0.5)
%   qsne(__,'ls_step', ls_step) line search step size (default: 0.5)
%   qsne(__,'callback', fn) callback function called after each iteration

function [Y, stat]= qsne(X, Y, p, h, iters, tol, varargin)
    %% Apply defaults
    if nargin < 3 || isempty(p)
        p= 30;
    end
    if nargin < 4 || isempty(h)
        h= 10;
    end
    if nargin < 5 || isempty(iters)
        iters= 1e3;
    end
    if nargin < 6 || isempty(tol)
        tol= 0;
    end
    
    %% Setup args
    aa= struct('kldival', 50, 'P', [], 'eta', 50, 'a1', 0.5, 'a2', 0.8, ...
        'iter_mswitch', 250, 'pscale', 4, 'damp', 0.8, 'ls_coef', 0.5, ...
        'ls_step', 0.5, 'callback', []);
    
    ff= fieldnames(aa);
    for i= 1:numel(ff)
        k= ff{i};
        ix1= find(strcmp(varargin, k));
        if ix1
            aa.(k)= varargin{ix1+1};
        end
    end
    
    %% Compute in dist, if not supplied
    if ~isempty(aa.P)
        P= aa.P;
        if ~isqmat(P)
            ld= ceil( size(P, 1) / 16 ) * 16;
            P1=nan(ld, 'single');
            P1(1:size(P,1), 1:size(P,1))=P;
            P=P1;
        end
    else
%         'Computing normal densities'
        P= make_normalp(qsne_copy(X, 0), p, size(X,1));
    end
    
    %% Setup data
    [m,c]= size(Y);
    ld= ceil(m/16)*16;
    elems= ld*c;
    psize=[ld c];
    % ringbuffers
    RG=nan(elems, h, 'single');
    RY=nan(elems, h, 'single');
    r_ix= 0;
    r_s= 0;
    ls_obj=realmax('double');
    ls_slope= 0;
    klds=inf(1, ceil(iters/aa.kldival));

    Q=nan(ld, ld, 'single');

    gains= ones(elems, 1);
    deltas= zeros(elems, 1);

    j= 1;
    kld= 0;
    alpha= 0;
    if h == 1
        alpha= aa.a1;
    end
    %% Exaggerate
    if aa.pscale > 1 && h == 1
       P= P*aa.pscale; 
    end
     
    %% Optimize
    Yp=qsne_copy(Y, 0);
    for i=1:iters
%     i
    % compute out dist
    [Q, s]= make_cauchyp(Yp, 1, m, Q);

    % do line search
    if aa.ls_coef > 0 && h > 1
        ls_a= 1;
        kld= compute_kld(P, Q);
        dd= reshape(deltas, psize);
        while ls_a > 0 && ~(kld <= (ls_obj + aa.ls_coef*ls_a*ls_slope))
            Yp= Yp-ls_a*dd + ls_a*aa.ls_step*dd;
            [Q,s]= make_cauchyp(Yp, 1, m, Q);
            kld= compute_kld(P, Q);
%            kld
            ls_a= ls_a*aa.ls_step;
%            ls_a
        end
    end
    
    if kld <= ls_obj && kld > 0
        ls_obj= kld;
    end
    
    % compute gradient
    G= cauchygradp(P, Q, Yp, m);
    
    % scale gradient
    G= 4*G*s;
    
    % put data to ringbuffer
    r_s= min(r_s+1, h);
    r_ix= rb_ind(h, r_ix, 1);
    
    RG(:, r_ix)= reshape(G, 1, [])';
    RY(:, r_ix)= reshape(Yp, 1, [])';
    % get update direction
    [~, S]= qsne_lbfgs_iter(RG, RY, single(r_ix), single(r_s), single(aa.damp));
    
    if i==aa.iter_mswitch && h == 1
%         'Switching momentum and removing exaggeration'
        alpha= aa.a2;
        if aa.pscale > 1
            P= P/aa.pscale;
        end
    end
    
    % get deltas
    deltas= do_update(S(:, 1), deltas, alpha, gains, aa.eta);
    
    % get ls slope
    if h > 1 && aa.ls_coef > 0
       ls_slope= sum(deltas .* RG(:,r_ix));
    end
    
    Yp(:)= Yp(:)+deltas;
    Y= Yp(1:m, :);
    
    if h == 1
       % center
       Y= Y - mean(Y);
    end
    
    if mod((i-1), aa.kldival) == 0
        kld= compute_kld(P, Q);
        klds(j)= kld;
        j = j+1;
%         kld
    end
    
    if ~isempty(aa.callback)
       aa.callback(Y); 
    end
    
    if j > 2 && (abs((klds(j-1)-klds(j-2))/(klds(j-2))) < tol) && i > aa.iter_mswitch
%         'Early exit'
        break
    end
    
    end
    
    stat= struct('kld', kld);
    
end

% get ring buffer index
function i= rb_ind(rb_size, rb_head, off)
    if (off < 0)
        i= (rb_size) - (mod((rb_size - rb_head - off), rb_size))+1;
    end
    i= mod(((rb_head-1) + off), rb_size)+1;
end

function deltas= do_update(S, deltas, alpha, gains, eta)
    if alpha > 0
        gains(gains > 0)= gains(gains > 0)*0.8;
        gains(gains <= 0)= gains(gains <= 0) + 0.2;
        gains(gains < 0.1)= 0.1;
        deltas= alpha*deltas - eta*gains.*S;
    else
        deltas= -S;
    end
end

function k= compute_kld(P, Q)
    S= P.*log(P./Q);
    k= sum(S(~isnan(S)));
end
