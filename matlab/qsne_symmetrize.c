
#include <mex.h>
#include <qsne.h>
#include <string.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isMatrix(const mxArray *arg)
{ return mxGetNumberOfDimensions(arg) == 2; }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (qsne_padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (qsne_padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_sigma"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *probs;
	mwSize rows, cols, j;
	float scale;
	float *out;

	/* Check input counts */
	if (nrhs < 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 1)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Check data */
	if (!(isFullReal(prhs[0]) && mxIsSingle(prhs[0]) && isMatrix(prhs[0]) && checkSinglePtr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":probs", "The input probs must be a padded full real single matrix.");

	/* Get data */
	probs = (const float *)mxGetData(prhs[0]);
	rows = mxGetM(prhs[0]);
	cols = mxGetN(prhs[0]);

	/* Check that we have a square matrix */
	if (rows != cols)
		mexErrMsgIdAndTxt(MEXFILENAME ":probs", "The input probs must be a square matrix.");

	/* Create output array */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(rows, cols, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	out = (float *)mxGetData(plhs[0]);
	memcpy(out, probs, rows * cols * sizeof(*out));
	qsne_symmetrize(out, rows, rows);
}
