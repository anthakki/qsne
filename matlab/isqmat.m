% ISQMAT Check if matrix is properly padded and in single precision
%   r = isqmat(X)
function r = isqmat(X)

r= isa(X, 'single') && ~mod(size(X, 1), 16);
end

