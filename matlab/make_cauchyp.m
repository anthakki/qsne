% MAKE_CAUCHYP Same as make_cauchy, but operate on padded matrices
%   [out_q, s]= make_cauchy(in_y, gamma, m, out_q)
% m = number of points
% out_q = output distribution

function [out_q, s]= make_cauchyp(in_y, gamma, m, out_q)
	
    assert(isqmat(in_y));
    assert(isqmat(out_q));
    d= qsne_pdist(in_y);
    q= out_q;
	
	d(1:size(d,1)+1:end)= inf;
    % put inf to padding
	d(m+1:end, :)= inf;
	d(:, m+1:end)= inf;
	
	%%
	s= 0;
	for j= 1:m
		[q(:, j), sj]= qsne_cauchy(d(:, j), single(gamma));
		assert(~isnan(sj));
		s= s + sj;
    end

    out_q= qsne_scale(q, 1/s); 
end