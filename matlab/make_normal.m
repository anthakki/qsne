% MAKE_NORMAl Compute P-matrix from pairwise distances
%   [out_p, tuned_perp]= make_normal(in_x, in_perp)
% Compute and transform pairwise distances for points in_x to symmetrized
% pairwise conditional gaussian probability matrix, with perplexity
% in_perp, and store the matrix in out_p. If in_perp is a range
% (two-element vector), tuned_perp will contain the adjusted perplexity
% value.
function [out_p, tuned_perp]= make_normal(in_x, in_perp)
    %%
    [m, ~]= size(in_x);
    d= qsne_pdist(qsne_copy(in_x));

    d1_core= squareform(pdist( in_x )) .^2;
    assert(all(reshape( abs(d(1:m, 1:m) - d1_core) <= sqrt(eps('single')) * abs(d1_core), [], 1)));

    % put inf to diagonal and padding
    d(1:size(d,1)+1:end)= inf;
    d(m+1:end, :)= inf;
    d(:, m+1:end)= inf;

    %%
    ents= num2cell(log(single(in_perp)));
    sigmas= qsne_sigma(d, ents{:});

    p= nan(size(d), 'like', d);
    s= 0;
    for j= 1:m
        [p(:, j), sj]= qsne_normal(d(:, j), sigmas(j));
        % TODO: this is present in Rtsne
        p(:, j)= p(:, j) / sj;
        sj= single(1);
        assert(~isnan(sj));
        s= s + sj;
    end

    tuned_perp= exp(-sum(xlogy(p(:, 1:m)), 1).');

    p(:)= qsne_scale(p, 1 / s);
    p(:)= qsne_symmetrize(p);

    out_p= nan(m, 'like', in_x);
    out_p(:)= p(1:m, 1:m);
