
% QSNE_COPY
%   y= QSNE_COPY(x)

function y= qsne_copy(x, padding, mode)
	%%
	if nargin < 2 || isempty(padding)
		padding= nan;
	end
	if nargin < 3 || isempty(mode)
		mode= 'rows';
	end
	
	%%
	if strcmp(mode, 'both')
		x= qsne_copy(x', padding)';
	end
	
	%%
	ld= ceil( size(x, 1) / 16 ) * 16;
	y= single(x);
	y(end+1:ld, :)= padding;
