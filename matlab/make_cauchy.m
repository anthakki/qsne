% MAKE_CAUCHY Compute Q-matrix from input points
%   [out_q, s]= make_cauchy(in_y, gamma)
%
% Compute pairwise distances for points in_y and transform them to
% conditional probabilities modeled using t-distribution. Setting 
% gamma=1 means Cauchy distribution. Q-matrix is stored in out_q and scale
% is returned in s.

function [out_q, s]= make_cauchy(in_y, gamma)
	%%
	if nargin < 2 || isempty(gamma)
		gamma= 1;
	end
	
	%%  
    m= size(in_y, 1);
    out_q= nan(m, 'like', in_y);
    d= qsne_pdist(qsne_copy(in_y));
    q= nan(size(d), 'like', d);
    
    d(1:size(d,1)+1:end)= inf;
    % put inf to padding
    d(m+1:end, :)= inf;
    d(:, m+1:end)= inf;
	
	%%
	s= 0;
	for j= 1:m
		[q(:, j), sj]= qsne_cauchy(d(:, j), single(gamma));
		assert(~isnan(sj));
		s= s + sj;
    end

    q(:)= qsne_scale(q, 1 / s);
    out_q(:)= q(1:m, 1:m);
end
