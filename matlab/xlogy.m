
function r= xlogy(x, y)
	%%
	if nargin < 2 || isempty(y)
		y= x;
	end
	
	r= x .* log(y);
% 	r(x == 0 & y == 0)= 0;
	r(x == 0 & (y | ~y))= 0;
