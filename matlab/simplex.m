
% SIMPLEX N-dimensional regular unit simplex
%   A= SIMPLEX(n) returns the n+1 vertices of the n-dimensional regular unit
%   simplex as columns of x. The simplex is centered to have the center of mass
%   at the origin.

function A= simplex(n)
	%%
	assert(isscalar(n) && isequal(n, fix(real(n))) && n >= 0, ...
		'The input N must be a non-negative real integer.');
	
	%%
	A= zeros(n, n+1);
	for i= 1:n
		% solve the last component of the vertex such that the distance from
		% origin has unit length
		A(i, i)= sqrt( 1^2 - sum( A(1:i-1, i).^2 ) );
		
		% solve the next component of the remaining vertices such that the
		% angle between A(:,i) and them is constant (arccos(-1/n))
		A(i, i+1:end)= A(i, i) \ (( -1/n - A(1:i-1, i)' * A(1:i-1, i+1:end) ));
	end
