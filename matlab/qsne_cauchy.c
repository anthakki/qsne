
#include <mex.h>
#include <qsne.h>

static
int
isFullReal(const mxArray *arg)
{ return !mxIsSparse(arg) && !mxIsComplex(arg); }

static
int
isColumn(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == mxGetM(arg); }

static
int
isScalar(const mxArray *arg)
{ return mxGetNumberOfElements(arg) == 1; } 

#define aligncount(ptr, type) \
	(((ptr) - (const type *)0) & (qsne_padcount(1u, sizeof(type)) - 1u))

static
int
checkSinglePtr(const mxArray *arg)
{
	mwSize ld;
	const float *data;

	/* Get properties */
	data = (const float *)mxGetData(arg);
	ld = mxGetM(arg);

#if 0
	/* Check alignment */
	if (!(aligncount(data, float) == 0))
		return 0;
#endif
	/* Check size */
	if (qsne_padcount(ld, sizeof(*data)) != ld)
		return 0;

	return 1;
}

#define MEXFILENAME "qsne_cauchy"

void
mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs)
{
	const float *dists;
	mwSize rows;
	float gamma, sum;

	/* Check input counts */
	if (nrhs < 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":minrhs", "Not enough input arguments");
	if (nrhs > 2)
		mexErrMsgIdAndTxt(MEXFILENAME ":maxrhs", "Too many input arguments");

	/* Check data */
	if (!(isFullReal(prhs[0]) && mxIsSingle(prhs[0]) && isColumn(prhs[0]) && checkSinglePtr(prhs[0])))
		mexErrMsgIdAndTxt(MEXFILENAME ":dists", "The input dists must be a padded full real single matrix.");
	if (!(isFullReal(prhs[1]) && mxIsSingle(prhs[1]) && isScalar(prhs[1])))
		mexErrMsgIdAndTxt(MEXFILENAME ":gamma", "The input gamma must be a single scalar.");

	/* Get data */
	dists = (const float *)mxGetData(prhs[0]);
	rows = mxGetM(prhs[0]);
	gamma = *(const float *)mxGetData(prhs[1]);

	/* Create output array */
	(void)nlhs;
	plhs[0] = mxCreateNumericMatrix(rows, 1, mxSINGLE_CLASS, mxREAL);
	plhs[1] = mxCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);

	/* Compute */
	sum = qsne_cauchy((float *)mxGetData(plhs[0]), dists, gamma, rows);
	*(float *)mxGetData(plhs[1]) = sum;
}
