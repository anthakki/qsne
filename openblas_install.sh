#!/usr/bin/env bash
git clone --depth 1 "https://github.com/xianyi/OpenBLAS.git"
cd OpenBLAS
PROCS=$(grep -c ^processor /proc/cpuinfo)
make -j $PROCS
